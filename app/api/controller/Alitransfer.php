<?php
/**
 * 转帐到支付宝账户demo
 * User: will
 * Date: 2021/4/22
  */
namespace app\api\controller;
use think\facade\Request;
use think\Db;
use think\Controller;
use think\facade\Cache;
use think\facade\Env;
require './vendor/alipay-sdk-php-all-master/aop/AopCertClient.php';
require './vendor/alipay-sdk-php-all-master/aop/request/AlipayFundTransUniTransferRequest.php';

class Alitransfer extends Controller
{      
        private $aop;
        /**
	 * @return [type] [description]
	 */
	public function initialize() {
//            var_dump($_SERVER['DOCUMENT_ROOT']);
//            die();
	    parent::initialize();
            $config = config('alipayApp');
            $this->aop = new \AopCertClient();
            $this->aop ->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $this->aop ->appId = "2021002117648882";
            $this->aop ->format='json';
            $this->aop ->charset='GBK';
            $this->aop->signType = 'RSA2';
            //$this->aop ->rsaPrivateKey = "";
            $this->aop ->rsaPrivateKey = "MIIEpAIBAAKCAQEAn2zb1ofxEKZ8IQfz9LZhg/t1pLIOR9I+vAzuf/IofuoAerMU2G3Qc9CFCmKhsrhLmGxkvlMRHPIOwDISGsiJHgDtC7qJOQkSuGZybAELvfw2jR+K5k5b8SRXL3ofoSp3Opu/KYpbs3uboC2zdlySniqgKxPxDHXp54dd1i1Bc0mHRwhPGX13EiMRVER+xPq/HGwu1Ovqm2bhHZiDZSwhxXwZbmAM2Tp7gGtzpMlecYwSWKUXSZL9KmebJJ3Qj7p6nWurwDJVr+r3KReNA8kyhia9CN05h9BPXzmfydYpdfN11KpWMgOigoN9NEZ1PxjdAPPINazcMlaRY64EV8h/zQIDAQABAoIBAQCeybMQsyfEuL2rvJscYscvyLZ9Cqe0m1DGxm6KWyxAdFtz2ohppL7Zi5fnJyP8uRj6QQP6mQvTc7iDb5uet5vymWOAqhNhDotpxQg0ULpcSSggWYlkKCnlPZfk/tMgGAbXBidZfagmVXhFCx/TRRL3nGcZYOxWx97oL2ikGUE2TDRSm4bEVm15LTwG8G4R6IYEiROiX3DPwFJFnkij01PRx7XQHlekFEovLCh6puWNbVob9L3MBLjDTamdi2Sm0jPNDAD832SGCV12E4lgCAFMSClg0moFuAXlmqQXmtz89N71Y+fvkiwR+BTDk+277uzuWu5hgOQgOWzOuE6xBFOBAoGBANFiZ6+dt/yySRJWJimHA8DH0CB7VE6Jw1ceOOoUuwgIEsWJ6AANPagZQJjN3HU044ly9IM0+dmY2FgVQFWXSpv8/Rn01TKBQhGtXI2zDH+m4FSmR7xbNlepfPUkeBB3FIiQwj+QnCz9TvdMRkWDIL4D0Wt2s1+GdpKyrRF39ZvhAoGBAMLrGF4xV2ZV0Q6s7xsVVPk4VyPrIerkz7QAQfHSkc1vgl/V0PSt+kyQO2EI7oSrzfKBDR+uKi+q3qQSEDkB+LyLb1uQMm8/yHcr0ofbYKq2q4zXxSvxptIy3bOhR/nZS8JO2WFPEGcfYpAcv0U9CAtb1w47fipPyo/OzzjDwUFtAoGAe4fsFk8dwmdI9xl+cFeoni8EmnVImwFqDtbUyz2pzqY8tPCUwJsPOkKTutMRctDeQTRiD5rMoVUspmL7TsPwj7Elfg/VTHO4EAfQqATvpcxFY4uqnPTPTc6/2z1F3Kj0o1GMWMYvbdeQpBQLDW28fn5wKk+gqgYBpLDnrHuymuECgYBEzrcAcUyYHI07MAMMPYCMpb3lORj1EabctQaSdjHwIoAUkqc76LqmEnhTrdJ9VSTTiYj3eQT9ZMVJh/LgHPkQnexGl2Wmbg63OuoL+X5rZSa0BmtdMuYf58LpqXJ+GZ4rzqfgOQfiT2r80pkWuQ/A6r/FD21fRGGOWIRAVqI/FQKBgQCY0hhEXTi4Q99+9ANqDW43mc0wVG7aj8A+RiNB6uIA5MK7Vip+U1xw9OfjE/gnKHZY46hlGoxklzHLJzGRvtkdNZQM31FUCER/EHqD1UXlLFjoz4CCuq247u2uYENuHFsQRnxV6lh7yZ7sZ9SOXCJpDcWx5l5Ftgu2ep1taQ6CKw==";
            $this->aop ->alipayrsaPublicKey = $this->aop->getPublicKey($_SERVER['DOCUMENT_ROOT']."/public/key/alipayCertPublicKey_RSA2.crt");
            $this->aop->isCheckAlipayPublicCert = true;
            $this->aop->appCertSN = $this->aop->getCertSN($_SERVER['DOCUMENT_ROOT']."/public/key/appCertPublicKey_2021002117648882.crt");
            $this->aop->alipayRootCertSN = $this->aop->getRootCertSN($_SERVER['DOCUMENT_ROOT']."/public/key/alipayRootCert.crt");
        }
        
        public function test(){
            //3、pageExecute 测试https://test.mszn1.com/api.php/alitransfer/test/
            $this->aop->apiVersion = '1.0';
            $request = new \AlipayFundTransUniTransferRequest();
            $testdata = [
                "out_biz_no"   => "2021051213000000003",
                "trans_amount" => "50",
                "product_code" => "TRANS_ACCOUNT_NO_PWD",
                "biz_scene"    => "DIRECT_TRANSFER",
                "payee_info"   => json_encode([
                                      "identity"      => "肖亚",
                                      "identity_type" => "ALIPAY_LOGON_ID",
                                      "name"          => "18565808837",
                                  ]),
                "order_title"  => "测试转账到支付宝账户",
            ];
            $request->setBizContent(json_encode($testdata));
            $result = $this->aop->execute($request);
            echo "<pre>";
            var_dump($result);
            echo "<hr>";
            $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            var_dump($responseNode);
        }
        
        
}