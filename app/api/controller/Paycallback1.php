<?php
namespace app\api\controller;
use app\common\model\OrderPay as OrderPayModel;
use think\Db;
use app\common\WeChat\Payment;
use app\common\WeChat\PayHtml;
use think\facade\Env;
/**
 *
 */
class Paycallback extends Db
{
	//支付回调处理 小程序
	public function notifyurl() {
		$result = Payment::notify();
		//$a = '{"appid":"wx3c53cddd8bdb6d97","bank_type":"CFT","cash_fee":"1","fee_type":"CNY","is_subscribe":"N","mch_id":"1502083051","nonce_str":"1hnexb6dvhmb6f5nbvbookup7vd3zz0p","openid":"o603m5e3jbHuTLyR_FKdrQcw9OKE","out_trade_no":"15438313187801","result_code":"SUCCESS","return_code":"SUCCESS","time_end":"20181115163628","total_fee":"1","trade_type":"JSAPI","transaction_id":"4200000213201811308372399182","message":"\u652f\u4ed8\u6210\u529f"}';
		//$result = json_decode($a,true);
        //log_result(Env::get('root_path').'upload/dede.txt','回调入口:::'.json_encode($result,JSON_UNESCAPED_UNICODE));
		if ( $result['return_code'] == 'SUCCESS' ) {
			$flag = OrderPayModel::handlePaySuccess($result);
			if ( $flag ) {
				exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
				//return reJsonMsg(200,'');
			}
		}
		//return reJsonMsg(500,'');
		exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
	}

    //支付回调处理 H5支付
    public function wxNotifyUrlOfHf()
    {
        $weixinConfig = config('wxHfPay');
        $wxPay = new PayHtml($weixinConfig['app_id'],$weixinConfig['mch_id'],$weixinConfig['notify_url'],$weixinConfig['api_key']);
        $result = $wxPay->notify();
        log_result('de.txt','$result::'.json_encode($result,JSON_UNESCAPED_UNICODE));
        /*
         * {
            "appid": "wxb5df31abdf85521e",
            "bank_type": "CFT",
            "cash_fee": "1",
            "fee_type": "CNY",
            "is_subscribe": "N",
            "mch_id": "1518565891",
            "nonce_str": "8hwi12bobREy4WtAWZNVzAv3oXE4Aq8G",
            "openid": "oXH0o4xjtN48wqTag1kO69v_9Q80",
            "out_trade_no": "201901270800548849400064",
            "result_code": "SUCCESS",
            "return_code": "SUCCESS",
            "time_end": "20190127200105",
            "total_fee": "1",
            "trade_type": "MWEB",
            "transaction_id": "4200000279201901275742442691"
        }
         */
        if ( !empty($result) && $result['result_code'] == 'SUCCESS' ) {
            $result = OrderPayModel::handleOrderPay($result['out_trade_no'],$result['transaction_id']);
            log_result('de.txt','22$result::'.json_encode($result,JSON_UNESCAPED_UNICODE));
        }
        exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
    }
    //支付回调处理 公众号支付
    public function wxNotifyUrlOfgzh()
    {
        $weixinConfig = config('wxJsapiPay');
        $wxPay = new PayHtml($weixinConfig['AppID'],$weixinConfig['MchId'],$weixinConfig['NotifyUrl'],$weixinConfig['ApiKey']);
        $result = $wxPay->notify();
        /*
         * {
            "appid": "wxb5df31abdf85521e",
            "bank_type": "CFT",
            "cash_fee": "1",
            "fee_type": "CNY",
            "is_subscribe": "N",
            "mch_id": "1518565891",
            "nonce_str": "8hwi12bobREy4WtAWZNVzAv3oXE4Aq8G",
            "openid": "oXH0o4xjtN48wqTag1kO69v_9Q80",
            "out_trade_no": "201901270800548849400064",
            "result_code": "SUCCESS",
            "return_code": "SUCCESS",
            "time_end": "20190127200105",
            "total_fee": "1",
            "trade_type": "MWEB",
            "transaction_id": "4200000279201901275742442691"
        }
         */
        if ( !empty($result) && $result['result_code'] == 'SUCCESS' ) {
            $result = OrderPayModel::handleOrderPay($result['out_trade_no'],$result['transaction_id']);
        }
        exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
    }
}
