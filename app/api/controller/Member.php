<?php

namespace app\api\controller;

use think\facade\Request;
use think\Db;
use app\common\model\OrderPay as OrderPayModel;
use app\common\model\DistributorAccount as DistributorAccountModel;
use app\common\model\Distributor as DistributorModel;
use app\common\model\DistributorEquipment as DistributorEquipmentModel;
use app\common\model\Config as ConfigModel;
use app\common\model\EquipmentPassword as EquipmentPasswordModel;
use app\common\model\User as UserModel;
class Member extends Base
{
	/**
	 * [memberLogin 分销商登陆]
	 * @return [type] [description]
	 */
	public function initialize() {
		parent::initialize();
	}
	public function memberLogin() {
		extract(input());
		if ( !$account )
			return reJsonMsg(500,'帐号不能为空');

		if ( !$password )
			return reJsonMsg(500,'密码不能为空');
		//验证手机号
		if ( !checkPhone($account) )
			return reJsonMsg(500,'手机号格式错误');

		//分销用户是否存在
		$distributor = model('Distributor')->where('d_user_phone',$account)->find();
		if ( !$distributor )
			return reJsonMsg(500,'无此分销商帐号'); 

		$password = pswCAM($account,$password);
		$mIsExist = model('User')->where(['u_phone'=>$account,'u_password'=>$password])->find();
		if ( !$mIsExist )
			return reJsonMsg(500,'帐号或密码错误');
		$user_id = $mIsExist->u_id;

		if ( !$this->authToken() )
			return reJsonMsg(100,'');	
		Db::startTrans();
		try {
			$userModel = model('User');
			//当前授权用户
			$userInfo = $userModel->find($this->user->u_id);
			if ( !empty($userInfo) ) {
				$data = array(
					'u_dis_uid' =>$user_id,
					'u_dis_id' => $distributor->d_id
				);
				$res = $userModel->where('u_id',$userInfo->u_id)->update($data);
				if ( $res ) {
					$userLoginLog = model('userLoginLog');
	                $data = array(
	                    'l_uid' =>$user_id,
	                    'l_uip' => getClientIP(),
	                    'l_utype'=>$userModel::$userTypeId,
	                    'l_token'=>setLoginToken($user_id),
	                    'l_login_time'=> date('Y-m-d H:i:s')
	                );
	                $userLoginLog->save($data);
				}
				Db::commit();
				$uInfo = $userModel->find($userInfo->u_id);
				return reJsonMsg(200,'',$uInfo);
			}
		} catch ( \Exception $e ) {
			Db::rollback();
			return reJsonMsg(500,'登陆失败');
		}
	}

	/**
	 * [distributorEarnings 分销商收益]
	 * @return [array] [description]
	 */
	public function distributorEarnings() {
		if ( !$this->authToken() )
			return reJsonMsg(100,'');
		$model = new OrderPayModel();

		$start = strtotime(date('Y-m-d').' 00:00:00');
		$end = strtotime(date('Y-m-d').' 23:59:59');
		$days = $model->where('o_dis_id',$this->user->u_dis_id)->where('o_paytime','between',array($start,$end))->select();

		$dayEarnings = 0.00;
		if ( !empty($days) ) {
			foreach ($days as $key => $value) {
				$dayEarnings += $value->o_price;
			}
		}
		$data = DistributorAccountModel::where('dis_id',$this->user->u_dis_id)->field('income,balance,withdraw,freeze')->find();
		if ( empty($data) )
			$income = $balance = $withdraw = $freeze = 0.00;
		else {
			$income = $data->income;
			$balance = $data->balance;
			$withdraw = $data->withdraw;
			$freeze = $data->freeze;
		}
		$list = array(
			'dayearnings' =>$dayEarnings.'.00',
			'income'=>$income ?? 0,
			'balance'=>$balance ?? 0,
			'withdraw'=>$withdraw ?? 0,
			'freeze' =>$freeze ?? 0
		);
		//分销商信息
		/*$disModel = DistributorModel::with('user')->find($this->disEquipment['d_dis_id']);
		$list['disinfo'] = $disModel;*/
		$disModel = DistributorModel::with('user')->where('d_id='.$this->user->u_dis_id)->find();
		$list['disinfo'] = $disModel;
		return reJsonMsg(200,'',$list);
	}

	/**
	 * [incomeCensus 当前分销商及各下级分销商的收益显示,按登陆的分销商帐号来显示]
	 * @return [array] [description]
	 */
	public function incomeCensus() {
		if ( !$this->authToken() )
			return reJsonMsg(100,'');

		//$disEquipment_id = $this->disEquipment['d_dis_id'];
		$member_account = input('member_account','');
		if ( empty($member_account) )
			return reJsonMsg(500,'帐户不能为空');


		$model = DistributorModel::where('d_user_phone',$member_account)->find();
		if ( empty($model) )
			return reJsonMsg(500,'未找到相关数据');

		//我的收益
		$myIncome = DistributorAccountModel::handleDistributorIncome($model->d_id);
		
		//最近30天内收益
		$leastThirtyIncome = DistributorAccountModel::handle30dayIncome($model->d_id);

		//下级分销商的收益
		$sonDistributor = DistributorModel::where('d_parent_id',$model->d_id)/*->where('d_parent_id','<>',0)*/->select();

		$kidDistributorArray = array();
		$total_count = array();
		$total_days = $total_heap = 0;
		if ( !empty($sonDistributor) ) {
			//说明有下级分销商
			foreach ($sonDistributor as $key => $value) {
				$kidDistributorArray[$key]['level'] = $value->d_type;
				$children = DistributorAccountModel::handleDistributorIncome($value->d_id);
				$kidDistributorArray[$key]['kid'] = $children; 
				$kidDistributorArray[$key]['name'] = $value->d_name;
				$kidDistributorArray[$key]['dis_id'] = $value->d_id;
				$total_days += $children[0];
				$total_heap += $children[1];
			}
			$total_days = $total_days + $myIncome[0];
			$total_heap = $total_heap + $myIncome[1];
		}
		//总收益
		array_push($total_count,$total_days,$total_heap);
		$data = array(
			'total'=>$total_count,
			'my'=>$myIncome,
			'thirty'=>$leastThirtyIncome,
			'children' =>$kidDistributorArray
		);
		return reJsonMsg(200,'',$data);
	}	

	/**
	 * [handleOneDistributorIncome 查看指定某个下级分销商或自己的交易记录]
	 * @return [array] [description]
	 */
	public function handleOneDistributorIncome() {

		if ( !$this->authToken() )
			return reJsonMsg(100,'');

		$disid = input('disid',0);
		$page  = input('page',1);
		$limit = input('limit',10);

		$model = OrderPayModel::where('o_line_number','<>',null)->order('o_paytime','DESC');
		if ( empty($disid) ) {
			//查看自己的交易记录
			$model->where('o_dis_id',$this->user->u_dis_id);
			
		} else {
			//查看下级的交易记录
			$model->where('o_dis_id',$disid);
		}

		$data = $model->page($page,$limit)->select();
		if ( !empty($data) ) {
			foreach ($data as $key => &$value) {
				$value['timelong'] = OrderPayModel::getEquTimeLong($value->o_type);
			}
		}
		return reJsonMsg(200,'',$data);
	}


	/**
	 * [initializePwd 分销商初始化密码]
	 * @return [type] [description]
	 */
	public function initializePwd() {
		if ( !$this->authToken() )
			return reJsonMsg(100,'');
		$member_account = input('member_account','');
		if ( empty($member_account) )
			return reJsonMsg(500,'帐户错误');
		Db::startTrans();
		try {
			$disEquipment = $this->disEquipment;
			/*$uinfo = UserModel::where('u_phone',$member_account)->find();*/
			if ( $this->user->u_dis_id  != $disEquipment['d_dis_id'] ) 
				return reJsonMsg(500,'抱歉，分销商帐号不匹配');
			$model = DistributorEquipmentModel::where(['d_dis_id'=>$disEquipment['d_dis_id'],'d_equipment_id'=>$disEquipment['d_equipment_id']])->find();
			if ( empty($model) )
				return reJsonMsg('500','未找到相关设备');

			$equipment = EquipmentPasswordModel::find(1);
			$flag = db('distributor_equipment')->where('d_id='.$model->d_id)->update(['d_equipment_pwd_prefix'=>$equipment->p_prefix]);
			if ( $flag ) {
				$m = OrderPayModel::where(['o_dis_id'=>$disEquipment['d_dis_id'],'o_dis_equipment_id'=>$disEquipment['d_id']])->order('o_paytime','DESC')->find();
				$m->o_equipment_pwd_prefix = $equipment->p_prefix;
				$m->o_starttime = $_SERVER['REQUEST_TIME'];
				//结束时间
            	$timer = ConfigModel::typeTransfer($m->o_type);
            	$m->o_endtime = strtotime("+".$timer.' hours');

            	$flag = $m->force()->save();
            	if ( $flag ) {
            		Db::commit();
            		return reJsonMsg(200,'初始化成功');
            	}
            	Db::rollback();
			}
			Db::rollback();			
		} catch ( \Exception $e ) {
			Db::rollback();
			echo $e->getMessage();
			return reJsonMsg(500,'初始化失败');
		}
	}
}