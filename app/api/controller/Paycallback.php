<?php

namespace app\api\controller;

use app\common\model\Config as ConfigModel;
use app\common\model\EquipmentLastPsd as EquipmentLastPsdModel;
use app\common\model\EquipmentPsdSet as EquipmentPsdSetModel;
use app\common\model\OrderPay as OrderPayModel;
use app\common\model\OrderPayYajin;
use think\Db;
use app\common\WeChat\Payment;
use app\common\WeChat\PayHtml;
use think\facade\Env;

/**
 *
 */
class Paycallback extends Db
{
    //支付回调处理 小程序
    public function notifyurl()
    {
        $result = Payment::notify();
        //$a = '{"appid":"wx3c53cddd8bdb6d97","bank_type":"CFT","cash_fee":"1","fee_type":"CNY","is_subscribe":"N","mch_id":"1502083051","nonce_str":"1hnexb6dvhmb6f5nbvbookup7vd3zz0p","openid":"o603m5e3jbHuTLyR_FKdrQcw9OKE","out_trade_no":"15438313187801","result_code":"SUCCESS","return_code":"SUCCESS","time_end":"20181115163628","total_fee":"1","trade_type":"JSAPI","transaction_id":"4200000213201811308372399182","message":"\u652f\u4ed8\u6210\u529f"}';
        //$result = json_decode($a,true);
        //log_result(Env::get('root_path').'upload/dede.txt','回调入口:::'.json_encode($result,JSON_UNESCAPED_UNICODE));
        if ($result['return_code'] == 'SUCCESS') {
            $flag = OrderPayModel::handlePaySuccess($result);
            if ($flag) {
                exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
                //return reJsonMsg(200,'');
            }
        }
        //return reJsonMsg(500,'');
        exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
    }

    //支付回调处理 H5支付
    public function wxNotifyUrlOfHf()
    {
        $weixinConfig = config('wxHfPay');
        $wxPay = new PayHtml($weixinConfig['app_id'], $weixinConfig['mch_id'], $weixinConfig['notify_url'], $weixinConfig['api_key']);
        $result = $wxPay->notify();
        //       log_result('zhifu2.txt','$result::'.json_encode($result,JSON_UNESCAPED_UNICODE));
        /*
         * {
            "appid": "wxb5df31abdf85521e",
            "bank_type": "CFT",
            "cash_fee": "1",
            "fee_type": "CNY",
            "is_subscribe": "N",
            "mch_id": "1518565891",
            "nonce_str": "8hwi12bobREy4WtAWZNVzAv3oXE4Aq8G",
            "openid": "oXH0o4xjtN48wqTag1kO69v_9Q80",
            "out_trade_no": "201901270800548849400064",
            "result_code": "SUCCESS",
            "return_code": "SUCCESS",
            "time_end": "20190127200105",
            "total_fee": "1",
            "trade_type": "MWEB",
            "transaction_id": "4200000279201901275742442691"
        }
         */
        if (!empty($result) && $result['result_code'] == 'SUCCESS') {
            $result = OrderPayModel::handleOrderPay($result['out_trade_no'], $result['transaction_id']);
        }
        exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
    }

    //支付回调处理 H5支付押金回调
    public function wxNotifyUrlOfHfYajin()
    {
        $weixinConfig = config('wxHfPay');
        $wxPay = new PayHtml($weixinConfig['app_id'], $weixinConfig['mch_id'], $weixinConfig['notify_url'], $weixinConfig['api_key']);
        $result = $wxPay->notify();

        if (!empty($result) && $result['result_code'] == 'SUCCESS') {
            $result = OrderPayYajin::handleOrderPayYajin($result['out_trade_no'], $result['transaction_id']);
        }
        exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
    }

    //支付回调处理 微信小程序支付
    public function wxMiniNotifyUrlOfHf()
    {
        $config = config('wxMiniPay');
        $wxPay = new PayHtml($config['AppID'], $config['MchId'], $config['NotifyUrl'], $config['ApiKey']);
        $result = $wxPay->notify();
        //       log_result('zhifu2.txt','$result::'.json_encode($result,JSON_UNESCAPED_UNICODE));
        /*
         * {
            "appid": "wxb5df31abdf85521e",
            "bank_type": "CFT",
            "cash_fee": "1",
            "fee_type": "CNY",
            "is_subscribe": "N",
            "mch_id": "1518565891",
            "nonce_str": "8hwi12bobREy4WtAWZNVzAv3oXE4Aq8G",
            "openid": "oXH0o4xjtN48wqTag1kO69v_9Q80",
            "out_trade_no": "201901270800548849400064",
            "result_code": "SUCCESS",
            "return_code": "SUCCESS",
            "time_end": "20190127200105",
            "total_fee": "1",
            "trade_type": "MWEB",
            "transaction_id": "4200000279201901275742442691"
        }
         */
        if (!empty($result) && $result['result_code'] == 'SUCCESS') {
            $result = OrderPayModel::handleOrderPay($result['out_trade_no'], $result['transaction_id']);
        }
        exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
    }

    //支付回调处理 支付宝支付
    public function alipayNotify()
    {
// 收到请求，先验证签名
        $result = (new \app\common\AliPay\AliPay())->notify();
        if ($result && $result['trade_status'] == 'TRADE_SUCCESS') {
            $result = OrderPayModel::handleOrderPay($result['out_trade_no'], $result['trade_no']);
        }
    }

    //押金支付回调处理 支付宝支付
    public function alipayNotifyYajin()
    {
// 收到请求，先验证签名
        $result = (new \app\common\AliPay\AliPay())->notify();
        log_result('zhifu3.txt', '$result::' . json_encode($result, JSON_UNESCAPED_UNICODE));
        if ($result && $result['trade_status'] == 'TRADE_SUCCESS') {
            log_result('zhifu3.txt', '$result::1');
            $result = OrderPayYajin::handleOrderPayYajin($result['out_trade_no'], $result['trade_no']);
        }
    }

    //支付回调处理 公众号支付
    public function wxNotifyUrlOfgzh()
    {
        $weixinConfig = config('wxJsapiPay');
        $wxPay = new PayHtml($weixinConfig['AppID'], $weixinConfig['MchId'], $weixinConfig['NotifyUrl'], $weixinConfig['ApiKey']);
        $result = $wxPay->notify();

        log_result('zhifu.txt', '$result::' . json_encode($result, JSON_UNESCAPED_UNICODE));

        if (!empty($result) && $result['result_code'] == 'SUCCESS') {
            $result = OrderPayModel::handleOrderPay($result['out_trade_no'], $result['transaction_id']);
        }
        exit('<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>');
    }


    //冻结押金支付
    public function alipayNotifyYajinFrozen()
    {
        $data = json_encode($_REQUEST) ?: file_get_contents('php://input');
        $data = json_decode($data, 1);
        Db::startTrans();
        log_result('zhifu.txt', '$result::' . json_encode($data, JSON_UNESCAPED_UNICODE));

        if (!$data) {
            return;
        }
        try {
            $orderdata = db("order_pay")->where(["o_number" => $data["out_order_no"]])->field("o_dis_equipment_number,o_psd_set_id,o_dis_id,o_equipment_pwd,o_type")->find();
            $orderwhere["o_number"] = $data["out_order_no"] ?? "";
            $orderwhere["pay_status"] = 0;
            $update["pay_status"] = 1;
            $update["o_line_number"] = $data["auth_no"] ?? "";
            $update["o_starttime"] = time();
            $update["o_paytime"] = time();
            $update["update_time"] = time();
            $update["o_endtime"] = time() + 86400;

            $bool = db("order_pay")->where($orderwhere)->update($update);
            if ($bool) {
                $orderdata = db("order_pay")->where(["o_number" => $data["out_order_no"]])->field("o_dis_equipment_number,o_psd_set_id,o_dis_id,o_equipment_pwd")->find();
                $eNumber = $orderdata['o_dis_equipment_number'];
                $psdId = $orderdata['o_psd_set_id'];
                $eDisId = $orderdata['o_dis_id'];
                $psd_value = $orderdata['o_equipment_pwd'];
                $lastPsdWhere = 'p_equipments_number="' . $eNumber . '" AND p_psd_set_id=' . $psdId . ' AND p_dis_id=' . $eDisId;
                if (EquipmentLastPsdModel::where($lastPsdWhere)->count() > 0) {
                    if (!EquipmentLastPsdModel::where($lastPsdWhere)->update(['p_psd_value' => $psd_value])) {
                    }
                } else {
                    $newData = [
                        'p_dis_id'            => $eDisId,
                        'p_psd_set_id'        => $psdId,
                        'p_psd_value'         => $psd_value,
                        'p_equipments_number' => $eNumber,
                        'p_addtime'           => myTime()
                    ];
                    $equipmentModle = new EquipmentLastPsdModel();
                    if (!$equipmentModle->insert($newData)) {
                        Db::rollback();
                        return false;
                    }
                }

                Db::commit();
                return true;
            }
        } catch (\Exception $e) {
            Db::rollback();
        }


    }
}
