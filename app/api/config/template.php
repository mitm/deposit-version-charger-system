<?php


return [
    // 视图输出字符串内容替换
    'tpl_replace_string'       => [
        '__PURL__' => '/public/static',
        '__CSS__' => '/public/static/admin/css',
        '__JS__'  => '/public/static/admin/js',
        '__IMG__' => '/public/static/admin/img',
        '__LAYUI__' => '/public/static/lib/layui',
        '__LAYUICSS__' => '/public/static/lib/layui/css',
        '__PJS__'  => '/public/static/js',
        '__PIMG__'  => '/public/static/img',
        '__VENDOR__'  => '/vendor/',
    ],
];