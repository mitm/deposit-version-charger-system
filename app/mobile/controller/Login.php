<?php
/**
 * wap登录类
 * User: jund
 * Date: 2019/1/19
 * Time: 23:17
 */
namespace app\mobile\controller;
use think\Controller;
use app\common\WeChat\Login as loginCom;
use app\common\WeChat\PayHtml;
class Login extends Controller
{

    public function index()
    {
        $this->assign('pageTitle','登录');
        return $this->fetch();
    }
    public function gotoLogin()
    {
        $account = input('account');
        $password = input('password');
        if (!$account) return reAjaxMsg(0,'请输入手机账号！');
        if (!$password) return reAjaxMsg(0,'请输入密码！');
        if (session('?userInfo.userAccount')) return reAjaxMsg(1,session('userInfo.userAccount').'：该用户已登录！');
        $userCom = controller('common/user');
        $login_resutle = $userCom->login($account,$password);
        if ($login_resutle != 'ok') return reAjaxMsg(0,$account."：".$login_resutle);
        return reAjaxMsg(1,'已成功登录！');
    }
    //微信登录
    public function gotoWxLogin()
    {
        $code = input('code','');
        $con = input('con','');//控制器名
        $dis_id = input('dis_id','');//分销商ID
        $configData = config('wxGongzhonghao');
        //获取微信用户信息
        $loginCom = new loginCom();
        $userInfo = $loginCom->getWxUserInfo($configData['AppID'], $configData['AppSecret'], $code);
        if ($userInfo) {
            $loginResult = controller('common/user')->speedyLogin($userInfo,1);
            if ($loginResult == 'ok'){
                $url = $con == 'Member' ? 'member/index' : 'index/index';
                $loginResult = '[000]授权失败！';
                $this->redirect(url($url,['con'=>$con,'dis_id'=>$dis_id]));
            } else {
                $loginResult = '[0001]授权失败！';
            }
        } else {
            $loginResult = '[03]授权失败！';
        }
        $this->assign('loginResult',$loginResult);
        return $this->fetch('speedyLogin');
    }
    //支付宝登陆
    public function gotoAlipayLogin()
    {
        $this->assign('pageTitle','登录');
        $code = input('auth_code','');
        $con = input('con','');//控制器名
        $dis_id = input('dis_id','');//分销商ID
        $userInfo = (new \app\common\AliPay\AliPay())->getUserInfo($code);
        if ($userInfo) {
            $loginResult = controller('common/user')->speedyAliPayLogin($userInfo,1);
            if ($loginResult == 'ok'){
                $url = $con == 'Member' ? 'member/index' : 'index/index';
                $loginResult = '[000]授权失败！';
                $this->redirect(url($url,['con'=>$con,'dis_id'=>$dis_id]));
            } else {
                $loginResult = '[0001]授权失败！';
            }
        } else {
            $loginResult = '[03]授权失败！';
        }
        $this->assign('loginResult',$loginResult);
        return $this->fetch('speedyLogin');
    }
    /*public function de(){
        $configData = config('wxJsapiPay');
        $trade_no = myTime('Ymdhis').getRandNen(10);
        $payHtml = new PayHtml($configData['AppID'],$configData['MchId'],$configData['NotifyUrl'],$configData['ApiKey']);
        $result = $payHtml->payToWeixin(0.30,$trade_no,$configData['AppID'],$configData['MchId'],$configData['ApiKey'],$configData['CertificatePath'],'oI0ug51ZSIC8_VcnlTluXGhG32eM');
        //payToWeixin($money, $trade_no, $appid, $mch_id, $mch_no, $openid, $certificatePath)
        return $result;
    }*/
    //返还抽佣
    /* public function de()
     {
         $result = db('distributor_account_detail')->where('dis_id=0 AND op_way=3 AND status=1')->select();
         foreach ($result as $vlist) {
             $money = $vlist['money'];
             $dis_id = $vlist['target_dis_id'];
             $mytime = myTime();
             if ($account = db('distributor_account')->where('dis_id='.$dis_id)->find()) {
                 $newbalance = bcadd($account['balance'],$vlist['money'],2);
                 $income = bcadd($account['income'],$vlist['money'],2);
                 db('distributor_account')->where('dis_id='.$dis_id)->update(['balance'=>$newbalance,'income'=>$income]);
             } else {
                 $newData = [
                     "dis_id" =>$dis_id,
                     "user_id" =>db('distributor')->where('d_id')->value('d_user_id'),
                     "income" =>$money,
                     "balance" =>$money,
                     "freeze" =>0,
                     "withdraw" =>0,
                     "create_time" =>$mytime,
                     "update_time" =>$mytime
                 ];
                 db('distributor_account')->insert($newData);
             }
             db('distributor_account_detail')->where('id='.$vlist['id'])->update(['status'=>-1]);
             $mytime = time();
             $newData1 = [
                 'dis_id' => $dis_id,
                 'op_way' =>4,
                 'money' =>$money,
                 'op_type' =>1,
                 'create_time' =>$mytime,
                 'update_time' =>$mytime,
                 'target_dis_id' =>0,
                 'target_dis_name' =>'平台',
                 'status' =>1,
             ];
             db('distributor_account_detail')->insert($newData1);
         }
    }*/
}
