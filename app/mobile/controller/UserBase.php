<?php
/**
 * 用户基类
 * User: jund
 * Date: 2018/7/24
 * Time: 15:51
 */

namespace app\mobile\controller;

use think\facade\Hook;
use app\index\controller\Base;
class UserBase extends Base
{
    public function initialize()
    {
        //判断用户是否登录
        Hook::listen('check_user_login',1);
    }

}