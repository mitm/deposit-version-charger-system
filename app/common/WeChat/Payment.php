<?php
/*
|--------------------------------------------------------------------------
| 微信支付API
|--------------------------------------------------------------------------
|
| @author Carmen
|
*/

namespace app\common\WeChat;
use app\common\WeChat\API;

class Payment
{   

    /**
     * [miniPay 小程序支付]
     * @param  [string] $body      [description]
     * @param  [string] $number    [description]
     * @param  [float] $fee       [description]
     * @param  [string] $notifyUrl [description]
     * @param  [array] $userInfo  [description]
     * @return [type]            [description]
     */
    public static function miniPay($body, $number, $fee, $notifyUrl,$userInfo) {
        $params = [
            'out_trade_no'     => $number,
            'body'             => $body,
            'total_fee'        => (double)bcmul($fee, 100),
            'notify_url'       => 'http://' . $_SERVER['HTTP_HOST'] . $notifyUrl,
            'trade_type'       => 'JSAPI',
            'openid'           => $userInfo['openid'],
            'appid'            => API::APP_ID,
            'mch_id'           => API::MCHID,
            'spbill_create_ip' => getClientIp(),
            'nonce_str'        => API::createNoncestr()
        ];
        
        $params['sign'] = API::createSign($params);

        $xml = API::array2xml($params);
        
        // 获取预支付ID
        $data = curl_post('https://api.mch.weixin.qq.com/pay/unifiedorder', $xml);
        $data = API::xml2array($data);
        if (!isset($data['prepay_id']))
            return false;

        $prepayId = $data['prepay_id'];

        // JSAPI 所需参数
        $time = time();
        $parameters = [
            'appId'     => API::APP_ID,
            'timeStamp' => "$time",
            'nonceStr'  => API::createNoncestr(),
            'package'   => "prepay_id=$prepayId",
            'signType'  => "MD5"
        ];
        $parameters['paySign'] = API::createSign($parameters);
        $parameters = json_encode($parameters);
        return $parameters;
    }
	/**
	 * 发起公众号支付
     *
	 * @param string 	$body        支付内容
	 * @param string 	$number      订单号
	 * @param double 	$fee         支付金额
	 * @param string 	$notifyUrl   异步回调地址
	 * @param string 	$successUrl  成功跳转地址
	 * @param string 	$errorUrl    失败跳转地址
	 */
	public static function ServicePay($body, $number, $fee, $notifyUrl, $successUrl, $errorUrl)
	{
		$params = [
            'out_trade_no'     => $number,
            'body'             => $body,
            'total_fee'        => (double) $fee * 100,
            'notify_url'       => 'http://' . $_SERVER['HTTP_HOST'] . $notifyUrl,
            'trade_type'       => 'JSAPI',
            'openid'           => API::getUserOpenid(),
            'appid'            => API::APP_ID,
            'mch_id'           => API::MCHID,
            'spbill_create_ip' => getClientIp(),
            'nonce_str'        => API::createNoncestr()
		];

		$params['sign'] = API::createSign($params);
		$xml = API::array2xml($params);

		// 获取预支付ID
        $data = \HttpResponse::post('https://api.mch.weixin.qq.com/pay/unifiedorder', $xml);
        $data = API::xml2array($data);

        if (!isset($data['prepay_id']))
            return false;

        $prepayId = $data['prepay_id'];

        // JSAPI 所需参数
        $time = time();
        $parameters = [
            'appId'     => API::APP_ID,
            'timeStamp' => "$time",
            'nonceStr'  => API::createNoncestr(),
            'package'   => "prepay_id=$prepayId",
            'signType'  => "MD5"
        ];
        $parameters['paySign'] = API::createSign($parameters);
        $parameters = json_encode($parameters);

        // 输出H5 JSAPI
        $str = <<<EOF
<!DOCTYPE html><html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <title>微信安全支付</title>
    <script type="text/javascript">
        function jsApiCall()
        {
            WeixinJSBridge.invoke(
                'getBrandWCPayRequest',
                {$parameters},
                function(res)
                {
                    if(res.err_msg == "get_brand_wcpay_request:ok")
                    {
                        window.location.href = '{$successUrl}';
                    }
                    else
                    {
                        window.location.href = '{$errorUrl}';
                    }
                }
            );
        }

        function callpay()
        {
            if (typeof WeixinJSBridge == "undefined"){
                if( document.addEventListener ){
                    document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
                }else if (document.attachEvent){
                    document.attachEvent('WeixinJSBridgeReady', jsApiCall);
                    document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
                }
            }else{
                jsApiCall();
            }
        }

        callpay();
    </script>
</head>
<body>

</body>
</html>
EOF;
        exit($str);
	}

    /**
     * 微信支付回调
     * @return array
     */
    public static function notify()
    {
        if (isset($GLOBALS['HTTP_RAW_POST_DATA']))
        {
            $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        }
        else
        {
            $xml = file_get_contents("php://input");
        }
        $data = API::xml2array($xml);
        $sign = $data['sign'];
        unset($data['sign']);

        if ($sign == API::createSign($data))
        {
            $returnXml = API::array2xml(['return_code' => 'SUCCESS']);
            if ($data["return_code"] == "FAIL")
            {
                $data['message'] = '通信出错';
            }
            else if ($data["result_code"] == "FAIL")
            {
                $data['message'] = '业务出错';
            }
            else
            {
                $data['message'] = '支付成功';
            }
        }
        else
        {
            $returnXml = API::array2xml(['return_code' => 'FAIL', 'return_msg'  => '签名失败']);
            $data = false;
        }
        // 返回结果给微信
        echo $returnXml;
        return $data;
    }
}
