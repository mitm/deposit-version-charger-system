<?php

namespace app\common\AliPay;

use think\facade\Env;

require_once 'function.inc.php';
require_once 'aop/AopClient.php';
require_once 'aop/AopCertClient.php';
require_once 'aop/request/AlipayUserUserinfoShareRequest.php';
require_once 'aop/request/AlipayUserUserinfoShareRequest.php';
require_once 'aop/request/AlipaySystemOauthTokenRequest.php';
require_once 'aop/request/AlipayTradeWapPayRequest.php';
require_once 'aop/request/AlipayTradeRefundRequest.php';
require_once 'aop/SignData.php';
require_once './vendor/alipay-sdk-php-all-master/aop/request/AlipayFundTransUniTransferRequest.php';

class AliPay
{
    public function getUserInfo($auth_code)
    {
        $token = $this->requestToken($auth_code);

        if (isset ($token->alipay_system_oauth_token_response)) {
            $token_str = $token->alipay_system_oauth_token_response->access_token;
            $user_info = $this->requestUserInfo($token_str);
//            object(stdClass)#39 (8) { ["code"]=> string(5) "10000" ["msg"]=> string(7) "Success" ["avatar"]=> string(72) "https://tfs.alipayobjects.com/images/partner/TB11uOYXFmb81Jjme7TXXc6FpXa" ["city"]=> string(9) "厦门市" ["gender"]=> string(1) "m" ["nick_name"]=> string(9) "仙士可" ["province"]=> string(9) "福建省" ["user_id"]=> string(16) "2088712427436710" }
            if (isset ($user_info->alipay_user_info_share_response)) {
                $user_info_resp = $user_info->alipay_user_info_share_response;
                return (array)$user_info_resp;
            }
        } elseif (isset ($token->error_response)) {
            // 返回了错误信息
            // 如：[code] => 40002
            // [msg] => Invalid Arguments
            // [sub_code] => isv.code-invalid
            // [sub_msg] => 授权码code无效

            // 记录错误返回信息
//            writeLog ( $token->error_response->sub_msg );
        }
//        writeLog ( var_export ( $token, true ) );
    }

    public function requestUserInfo($token)
    {
        $AlipayUserUserinfoShareRequest = new \AlipayUserUserinfoShareRequest ();
        $AlipayUserUserinfoShareRequest->setProdCode($token);

        $result = aopclient_request_execute($AlipayUserUserinfoShareRequest, $token);
        return $result;
    }

    public function requestToken($auth_code)
    {
        $AlipaySystemOauthTokenRequest = new \AlipaySystemOauthTokenRequest ();
        $AlipaySystemOauthTokenRequest->setCode($auth_code);
        $AlipaySystemOauthTokenRequest->setGrantType("authorization_code");

        $result = aopclient_request_execute($AlipaySystemOauthTokenRequest);
        return $result;
    }

    public function pay($authToken, $orderNumber, $price)
    {
        $config = config('alipayApp');

        $aop = new \AopCertClient();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->alipayrsaPublicKey = $aop->getPublicKey(Env::get('root_path') . 'upload/ali/alipayCertPublicKey_RSA2.crt');
        $aop->appCertSN = $aop->getCertSN(Env::get('root_path') . 'upload/ali/appCertPublicKey_2021002117684078.crt');
        $aop->alipayRootCertSN = $aop->getRootCertSN(Env::get('root_path') . 'upload/ali/alipayRootCert.crt');
        $aop->rsaPrivateKey = $config['merchant_private_key'];
        $aop->isCheckAlipayPublicCert = true;
        $aop->appId = $config["app_id"];

        $aop->signType = $config['sign_type'];
        $aop->apiVersion = "1.0";
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \AlipayTradeWapPayRequest();
        $request->setNotifyUrl($config['notify_url_yajin']);
        $request->setReturnUrl($_SERVER["HTTP_REFERER"]);
        $data = [
            'body'         => 'chongdian',
            'subject'      => $orderNumber,
            'out_trade_no' => $orderNumber,
            'total_amount' => $price,
            'auth_token'   => $authToken,
//            'quit_url'     => config('system_url') . "/mobile.php/index/index",
            'product_code' => 'QUICK_WAP_WAY',
        ];
        $request->setBizContent(json_encode($data));
        $result = $aop->pageExecute($request);
        return $result;
    }


    public function yajinPay($authToken, $orderNumber, $price)
    {
        $config = config('alipayApp');

        $aop = new \AopCertClient();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->alipayrsaPublicKey = $aop->getPublicKey(Env::get('root_path') . 'upload/ali/alipayCertPublicKey_RSA2.crt');
        $aop->appCertSN = $aop->getCertSN(Env::get('root_path') . 'upload/ali/appCertPublicKey_2021002117684078.crt');
        $aop->alipayRootCertSN = $aop->getRootCertSN(Env::get('root_path') . 'upload/ali/alipayRootCert.crt');
        $aop->rsaPrivateKey = $config['merchant_private_key'];
        $aop->isCheckAlipayPublicCert = true;
        $aop->appId = $config["app_id"];

        $aop->signType = $config['sign_type'];
        $aop->apiVersion = "1.0";
        $aop->postCharset = 'GBK';
        $aop->format = 'json';
        $request = new \AlipayTradeWapPayRequest();
        $request->setNotifyUrl($config['notify_url']);
        $request->setReturnUrl($_SERVER["HTTP_REFERER"]);
        $data = [
            'body'         => 'chongdian',
            'subject'      => $orderNumber,
            'out_trade_no' => $orderNumber,
            'total_amount' => $price,
            'auth_token'   => $authToken,
//            'quit_url'     => config('system_url') . "/mobile.php/index/index",
            'product_code' => 'QUICK_WAP_WAY',
        ];
        $request->setBizContent(json_encode($data));
        $result = $aop->pageExecute($request);
        return $result;
    }


    public function refund($authToken, $orderNumber, $refund_amount)
    {
        $config = config('alipayApp');

        $aop = new \AopCertClient();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->alipayrsaPublicKey = $aop->getPublicKey(Env::get('root_path') . 'upload/ali/alipayCertPublicKey_RSA2.crt');
        $aop->appCertSN = $aop->getCertSN(Env::get('root_path') . 'upload/ali/appCertPublicKey_2021002117684078.crt');
        $aop->alipayRootCertSN = $aop->getRootCertSN(Env::get('root_path') . 'upload/ali/alipayRootCert.crt');
        $aop->rsaPrivateKey = $config['merchant_private_key'];
        $aop->isCheckAlipayPublicCert = true;
        $aop->appId = $config["app_id"];
        $aop->signType = $config['sign_type'];
        $aop->apiVersion = "1.0";
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \AlipayTradeRefundRequest();
        //      $request->setNotifyUrl($config['notify_url']);
        //      $request->setReturnUrl($_SERVER["HTTP_REFERER"]);
        $data = [
            'out_trade_no'  => $orderNumber,
            'refund_amount' => $refund_amount,
            //       'auth_token'   => $authToken,

//            'quit_url'     => config('system_url') . "/mobile.php/index/index",

        ];
        $request->setBizContent(json_encode($data));

        $result = $aop->execute($request);
        $results = array();
        $results['id'] = $orderNumber;

        return $results;
    }

    public function transfer($loginId, $orderNumber, $amount, $note = "订单退款")
    {
        $config = config('alipayApp');
//        merchant_private_key
//merchant_public_key
//alipay_public_key
        $aop = new \AopCertClient();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->alipayrsaPublicKey = $aop->getPublicKey(Env::get('root_path') . 'upload/ali/alipayCertPublicKey_RSA2.crt');
        $aop->appCertSN = $aop->getCertSN(Env::get('root_path') . 'upload/ali/appCertPublicKey_2021002117684078.crt');
        $aop->alipayRootCertSN = $aop->getRootCertSN(Env::get('root_path') . 'upload/ali/alipayRootCert.crt');
        $aop->rsaPrivateKey = $config['merchant_private_key'];
        $aop->isCheckAlipayPublicCert = true;
        $aop->signType = $config['sign_type'];
        $aop->apiVersion = "1.0";
        $aop->appId = $config['app_id'];
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \AlipayFundTransUniTransferRequest();
//var_dump($config);
        $data = [
            "out_biz_no"   => $orderNumber,
            "trans_amount" => $amount,
            "product_code" => "TRANS_ACCOUNT_NO_PWD",
            "biz_scene"    => "DIRECT_TRANSFER",
            "payee_info"   => json_encode([
                "identity"      => $loginId,
                "identity_type" => "ALIPAY_USER_ID",
            ]),
            "order_title"  => $note,
        ];
        $request->setBizContent(json_encode($data));
        $result = $aop->execute($request);
        $result = $result->alipay_fund_trans_uni_transfer_response;
        if($result->code=='10000'){
            return ['id'=>1];
        }else{
            return ['id'=>0,'msg'=>$result->msg];
        }
        return $result;
    }


    function notify()
    {
        $config = config('alipayApp');
        $aop = new \AopCertClient();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->alipayrsaPublicKey = $aop->getPublicKey(Env::get('root_path') . 'upload/ali/alipayCertPublicKey_RSA2.crt');
        $aop->appCertSN = $aop->getCertSN(Env::get('root_path') . 'upload/ali/appCertPublicKey_2021002117684078.crt');
        $aop->alipayRootCertSN = $aop->getRootCertSN(Env::get('root_path') . 'upload/ali/alipayRootCert.crt');
        $aop->rsaPrivateKey = $config['merchant_private_key'];
        $aop->isCheckAlipayPublicCert = true;
        $aop->appId = $config["app_id"];

//待签名字符串
//签名方式
        $sign_type = "RSA2";
        $_POST = file_get_contents('php://input', 'r');
        $_POST = urldecode($_POST);
//把字符串通过&符号拆分成数组
        $data = explode('&', $_POST);
        $params = array();
//遍历数组
        foreach ($data as $param) {
            $item = explode('=', $param, "2");
            $params[$item[0]] = $item[1];
        }
//输出拆分后的数据
//print_r($params);
//验签代码
        $flag = $aop->rsaCheckV1($params, null, $sign_type);

//输出验签结果
        if ($flag) {
            echo "success";
            return $params;
        } else {
            echo "fail";
            return null;
        }
    }
}
