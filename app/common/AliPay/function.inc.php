<?php

use think\facade\Env;

function writeLog($text)
{
    // $text=iconv("GBK", "UTF-8//IGNORE", $text);
    $text = characet($text);
    file_put_contents(dirname(__FILE__) . "/log/log.txt", date("Y-m-d H:i:s") . "  " . $text . "\r\n", FILE_APPEND);
}

//转换编码
function characet($data)
{
    if (!empty ($data)) {
        $fileType = mb_detect_encoding($data, array(
            'UTF-8',
            'GBK',
            'GB2312',
            'LATIN1',
            'BIG5'
        ));
        if ($fileType != 'UTF-8') {
            $data = mb_convert_encoding($data, 'UTF-8', $fileType);
        }
    }
    return $data;
}

/**
 * 使用SDK执行接口请求
 * @param unknown $request
 * @param string  $token
 * @return Ambigous <boolean, mixed>
 */
function aopclient_request_execute($request, $token = NULL)
{
    $config = config('alipayApp');

    $aop = new \AopCertClient();
    $aop->gatewayUrl = $config ['gatewayUrl'];
    $aop->appId = $config ['app_id'];
    $aop->rsaPrivateKey = $config['merchant_private_key'];
    $aop->alipayrsaPublicKey = $config['alipay_public_key'];
    $aop->signType = $config['sign_type'];
    $aop->apiVersion = "1.0";
    $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
    $aop->alipayrsaPublicKey = $aop->getPublicKey(Env::get('root_path') . 'upload/ali/alipayCertPublicKey_RSA2.crt');
    $aop->appCertSN = $aop->getCertSN(Env::get('root_path') . 'upload/ali/appCertPublicKey_2021002117684078.crt');
    $aop->alipayRootCertSN = $aop->getRootCertSN(Env::get('root_path') . 'upload/ali/alipayRootCert.crt');
    $aop->rsaPrivateKey = $config['merchant_private_key'];
    $aop->isCheckAlipayPublicCert = true;
    $result = $aop->execute($request, $token);
//	writeLog("response: ".var_export($result,true));
    return $result;
}
