<?php
/**
 * 会员登录日志
 * User: jund
 * Date: 2018/7/30
 * Time: 15:51
 */

namespace app\common\model;


use think\Model;

class UserLoginLog extends Model
{
    protected $table = 'de_user_login_log';
    protected $pk = 'l_id';

    //获取会员登录日志分页
    public function getUserLoginLogPage($where, $fields = '*', $pageNum = 20, $order = 'l_login_time DESC', $query = [], $expire = 86400, $reSql = false)
    {
        $userWhere = isset($where['userWhere']) ? $where['userWhere'] : '';
        $where = isset($where['where']) ? $where['where'] : $where;
        $res = $this->hasWhere('User',$userWhere)->with(['User'=>function($query){
            $query->withField('u_id,u_name,u_phone');
        }])->where($where)->order($order)->fetchsql($reSql)->paginate($pageNum,false,$query)->each(
            function($vlist)
            {
                $vlist['l_utype_name'] = isset($vlist['l_utype']) ? model('User')->userType($vlist['l_utype']) : '';
            });
        return $res;
    }

    //一对一管理用户数据表
    public function user()
    {
        return $this->hasOne('User','u_id','l_uid');
    }
}
