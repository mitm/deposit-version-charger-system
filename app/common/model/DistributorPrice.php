<?php
/**
 * Created by PhpStorm.
 * User: jund
 * Date: 2019/3/28
 * Time: 23:25
 */

namespace app\common\model;


use think\Model;

class DistributorPrice extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'de_distributor_price';
    protected $pk = 'p_id';
}
