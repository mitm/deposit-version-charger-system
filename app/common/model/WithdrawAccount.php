<?php

namespace app\common\model;
use think\Model;
/**
 * 
 */
class WithdrawAccount extends Model
{
	
	// 设置当前模型对应的完整数据表名称
    protected $table = 'de_withdraw_account';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
}
