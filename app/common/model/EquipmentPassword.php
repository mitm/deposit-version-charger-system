<?php


namespace app\common\model;

use think\Model;
class EquipmentPassword extends Model
{
	// 设置当前模型对应的完整数据表名称
    protected $table = 'de_equipment_possword';
    protected $pk = 'p_id';

    /**
     * [handleSortPwdExt 处理密码后缀]
     * @param  [int] $prefix       [前缀]
     * @param  [int] $total_hour   [总小时]
     * @param  [int] $balance_hour [剩余小时]
     * @return [string]               [密码后缀]
     */
    public static function handleSortPwdExt($prefix,$total_hour,$balance_hour) {
    	if ( empty($prefix) || empty($total_hour) ) {
    		return false;
    	}
        //$h = $total_hour - $balance_hour;
        //var_dump($h);
        if ( $balance_hour <= 0 ) {
            return 0;
        }
    	$model = self::where(['p_prefix'=>$prefix])->field('p_pwd'.$balance_hour)->find();
    	return $model['p_pwd'.$balance_hour];
    }	
}