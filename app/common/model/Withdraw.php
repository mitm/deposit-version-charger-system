<?php
/**
 * 提现表
 */
namespace app\common\model;

use think\Model;
use app\common\model\DistributorAccount as DistributorAccountModel;
use app\common\model\DistributorAccountDetail as DistributorAccountDetailModel;
use think\Db;
use app\common\model\UserLog as UserLogModel;
use app\common\model\DistributorPrecentage as DistributorPrecentageModel;
use app\common\model\Distributor as DistributorModel;
use app\common\model\withDraw as withDrawModel;
class Withdraw extends Model
{

	protected $table = 'de_withdraw';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;

    CONST STATUS_CHECKED = 1;
    CONST STATUS_FAILED = -1;
    CONST STATUS_SUCCESSED = 2;

    /**
     * [handleWithdrawFreeze description]
     * @param  [int] $d_dis_id  [description]
     * @param  [float] $yongjing  [description]
     * @param  [float] $real_take [description]
     * @param  [float] $total     [description]
     * @return [bool]            [description]
     */
    public static function handleWithdrawFreeze($d_dis_id,$yongjing,$real_take,$total,$user_id) {

    	Db::startTrans();
    	try {
            //获取提现手续费百分比
            $withdrawValue = db('config')->where('c_id=1')->value('c_withdraw');
    		//入帐提现表
    		$data = array(
    			'dis_id' => $d_dis_id,
                'user_id'=>$user_id,
    			'get_cash' => $total,
    			'real_take_cash' => $withdrawValue && is_numeric($withdrawValue) && $withdrawValue >= 0.01 ? bcmul($total,bcsub(1,bcdiv($withdrawValue,100,2),2),2) : $total,
    			'prenceage' =>bcmul($total,bcdiv($withdrawValue,100,2),2)
    		);
    		$result = self::create($data);
    		if ( $result ) {
    			//处理冻结金额
    			$model = DistributorAccountModel::where('dis_id',$d_dis_id)->find();
    			if ( empty($model) ) return array('没有此分销商帐户');
                if ( $model->balance < $total ) return array('余额不可小于提现金额');
                //更新
                $model->user_id = $user_id;
                $model->balance = $model->balance - $total;
                $model->withdraw = $model->withdraw + $real_take;
                $model->freeze = $model->freeze + $total;
                $flag = $model->save();
    			if ( $flag ) {
    				Db::commit();
    				return (bool)$flag;
    			}
    		}
    		Db::rollback();
    		return false;
    	} catch ( \Exception $e ) {
    		Db::rollback();
    		echo $e->getMessage();
    		return false;
    	}
    }

    /**
     * [handelFreezeToAdmin 后台处理提现操作]
     * @param  [type] $dis_id  [description]
     * @param  [type] $getCash [description]
     * @return [type]          [description]
     */
    public static function handelFreezeToAdmin($id,$dis_id,$getCash,$status,$adminid) {
         //处理冻结金额释放操作
        $data = DistributorAccountModel::where('dis_id',$dis_id)->find();
        if ( $status == self::STATUS_SUCCESSED) {
            $data->freeze = $data->freeze - $getCash;
        } else {
            $data->withdraw = bcsub($data->withdraw,$getCash,2);
            $data->balance = bcadd($data->balance,$getCash,2);
            $data->freeze = bcsub($data->freeze,$getCash,2);
        }
        return $data->save();
    }

    /**
     * [handleWithdrawToAdmin 系统后台提现 管理员操作日志]
     * @param  [type] $userId     [用户id]
     * @param  [type] $userName   [用户名]
     * @param  [type] $beforedata [之前数据]
     * @param  [type] $afterdata  [之后数据]
     * @return [type]             [布尔]
     */
    public static function handleWithdrawToAdmin($userId,$userName,$beforedata,$afterdata) {
        if ( empty($userId) || empty($userName) || empty($beforedata) || empty($afterdata) )
            return false;
        //进入操作日志
        $l = array(
            'l_uid' => $userId,
            'l_username'=>$userName,
            'l_olddate'=>json_encode($beforedata),
            'l_newdata'=>json_encode($afterdata),
            'l_addtime'=>myTime()
        );
        return (bool)UserLogModel::create($l);
    }

     public function getBankAttr($value,$data){
        $bank = WithdrawAccount::where('user_id',$data['user_id'])->find();
        return $bank;
    }
}
