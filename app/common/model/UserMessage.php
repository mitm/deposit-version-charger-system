<?php
/**
 * 消息类
 * User: jund
 * Date: 2018/9/1
 * Time: 17:13
 */

namespace app\common\model;


use think\Model;

class UserMessage extends Model
{
    protected $table = 'de_user_message';
    protected $pk = 'm_id';

    CONST OPTYPE_DEAL = 1;		//处理中
    CONST OPTYPE_HANDLED = 2;	//已处理(回复)
    CONST OPTYPE_PASSED = 3;	//已忽略

    CONST DEL_USER_STATUS = 0;
    CONST DEL_SALE_STATUS = -1;
    CONST STATUS_OK = 1;
}