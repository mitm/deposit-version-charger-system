<?php

namespace app\common\model;

use think\Model;
class Config extends Model
{

	// 设置当前模型对应的完整数据表名称
    protected $table = 'de_config';
    protected $pk = 'o_id';
   // protected $autoWriteTimestamp = true;

    public function initialize() {

    }

    /**
     * [typeTransfer 类型转小时]
     * @param  [int] $type [description]
     * @return [int]       [description]
     */
    public static function typeTransfer($type) {
        $configInfo = db('config')->where('c_id=1')->find();
        switch ($type) {
            case 1:
                $timer = $configInfo['c_time1'];
                break;
            case 2:
                $timer = $configInfo['c_time2'];
                break;
            case 3:
                $timer = $configInfo['c_time3'];
                break;
        }
        return $timer;
    }

    //获取系统密码后缀
    public static function getSystemPwdExt($type) {
        $configInfo = db('config')->where('c_id=1')->find();
        switch ($type) {
            case 1:
                $ext = $configInfo['c_pwd1'];
                break;
            case 2:
                $ext = $configInfo['c_pwd2'];
                break;
            case 3:
                $ext = $configInfo['c_pwd3'];
                break;
        }
        return $ext;
    }

    //获取密码
    public static function getSystemPrice($type) {
        $configInfo = db('config')->where('c_id=1')->find();
        switch ($type) {
            case 1:
                $data['o_price'] = $configInfo['c_price1'];
                break;
            case 2:
                $data['o_price'] = $configInfo['c_price2'];
                break;
            case 3:
                $data['o_price'] = $configInfo['c_price3'];
                break;
        }
        return $data['o_price'];
    }
}
