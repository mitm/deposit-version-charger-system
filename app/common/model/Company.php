<?php
/**
 * 企业信息
 * User: jund
 * Date: 2018/8/14
 * Time: 15:26
 */

namespace app\common\model;


use think\Model;

class Company extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'de_company';
    protected $pk = 'c_id';
}