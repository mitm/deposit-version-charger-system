<?php
/**
 * 资讯类
 * User: jund
 * Date: 2018/9/4
 * Time: 18:10
 */

namespace app\common\model;


use think\Model;

class UserInformation extends Model
{
    protected $table = 'de_user_information';
    protected $pk = 'i_id';
}