<?php


namespace app\common\model;

use think\Model;
class DistributorEquipment extends Model
{
	// 设置当前模型对应的完整数据表名称
    protected $table = 'de_distributor_equipment';
    protected $pk = 'd_id';
}