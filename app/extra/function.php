<?php

//前端异步返回数组
function reAjaxMsg($id, $msg='', $info=''){
    return array('id'=>$id, 'msg'=>$msg, 'info'=> $info);
}
//前端异步返回数组
function reAjaxJsonMsg($id, $msg='', $info=''){
    return json_encode(array('id'=>$id, 'msg'=>$msg, 'info'=> $info));
}
/**
 * 前端异步放回json数据
 * 对数组变量进行 JSON 编码
 * @param mixed array 待编码的 array (除了resource 类型之外，可以为任何数据类型，该函数只能接受 UTF-8 编码的数据)
 * @return string (返回 array 值的 JSON 形式)
 */
function reJsonMsg($id, $msg='', $info=''){
    $array = array('id'=>$id, 'msg'=>$msg, 'info'=> $info);
    if(version_compare(PHP_VERSION,'5.4.0','<')){
        $str = json_encode($array);
        $str = preg_replace_callback("#\\\u([0-9a-f]{4})#i",function($matchs){
            return iconv('UCS-2BE', 'UTF-8', pack('H4', $matchs[1]));
        },$str);

        // 返回可执行的js脚本
//        header('Content-Type:text/html; charset=utf-8');
//        exit($str);
        return $str;
    }else{
        // 返回可执行的js脚本
        /*header('Content-Type:text/html; charset=utf-8');
        exit(json_encode($array, JSON_UNESCAPED_UNICODE));*/
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}
//时间戳转成日期
function myTime($date = 'Y-m-d H:i:s'){
    return date($date,$_SERVER['REQUEST_TIME']);
}
//获取用户ID
function getIp(){
    return request()->ip();
}
//检测是不是手机格式，是则返回true，否则返回false
function isPhone($phone){
    if (!is_numeric($phone)) {
        return false;
    }
    return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $phone) ? true : false;
}
//密码加密
function pswCAM($account,$password){
    return md5($account).crypt(md5(md5($password)),'de');
}
//设置登录token 用于APP或其他API接口登录
function setLoginToken($value) {
    $str = md5(uniqid(md5(microtime(true)), true));
    $str = sha1($str.$value);
    return $str;
}
//删除文件
function delFile($pic_name = ''){
    if (!$pic_name) return false;
    return is_file($pic_name) && unlink($pic_name);
}
//判断访客是手机还是PC
function checkIfPhone() {
    $isPhone = false;
    $user_agent = $_SERVER['HTTP_USER_AGENT'];//返回手机系统、型号信息
    $userUrl = $_SERVER['SERVER_NAME'];
    if(stristr($user_agent,'iPad')) {//返回值中是否有 iPad 这个关键字
        $isPhone = true;
    } else if(stristr($user_agent,'Android')) {//返回值中是否有Android这个关键字
        $isPhone = true;
    } else if(stristr($user_agent,'iPhone')) {
        $isPhone = true;
    } else if(stristr($user_agent,'Windows Phone')) { //你使用的是Windows Phone系统;
        $isPhone = true;
    } else if(stristr($userUrl,'wap')) { //你使用的是wap域名;
        $isPhone = true;
    }
    return $isPhone;
}
//判断是否微信浏览器
function isWeixin(){
    if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') === false ) {
        return false;
    }
    return true;
    //MicroMessenger 是android/iphone版微信所带的
    //Windows Phone 是winphone版微信带的  (这个标识会误伤winphone普通浏览器的访问)
    /*$ua = $_SERVER['HTTP_USER_AGENT'];
    return strpos($ua, 'MicroMessenger') === false || strpos($ua, 'Windows Phone') === false ? false : true;*/
}
//检测手机号
function checkPhone($phoneStr) {
    $exp = "/^13[0-9]{1}[0-9]{8}$|14[0-9]{1}[0-9]{8}$|15[0123456789]{1}[0-9]{8}$|17[0123456789]{1}[0-9]{8}$|18[0123456789]{1}[0-9]{8}$/";
    return preg_match($exp, $phoneStr);
}
//手机号码设置中间四位数字隐藏
function resetUserPhone($phone){
    if (!checkPhone($phone)) return false;
    return substr($phone,0,4)."****".substr($phone,-3);
}
// 座机验证
function checkTelphone($phone){
    $isTel="/^([0-9]{3,4}-)?[0-9]{7,8}$/";
    return preg_match($isTel, $phone);
}
//判断是不是邮箱格式
function isEmail($u_email){
    $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
    if (!preg_match($pattern, $u_email)) {
        return  false;
    } else {
        return true;
    }
    // return  preg_match("/^[\\w\\-\\.]+@[\\w\\-\\.]+(\\.\\w+)+$/", $n);
}
//生成文本记录，调试用
function log_result($file,$word) {
    $fp = fopen($file,"a");
    flock($fp, LOCK_EX) ;
    fwrite($fp,strftime("%Y-%m-%d-%H：%M：%S",time())."\n".$word."\n\n");
    flock($fp, LOCK_UN);
    fclose($fp);
}
//下面是生成token方法代码
function settoken()
{
    $str = md5(uniqid(md5(microtime(true)),true));  //生成一个不会重复的字符串
    $str = sha1($str);  //加密
    return $str;
}

// ------------ 字符串处理函数 -------------------------------------------------------------- //
//生成编号
function getGrowingNumberArr($startV = 0, $endV = 10, $len = 6){
    //设置从0开始
    $numberArr = [];
    for($i = $startV; $i < $endV; $i++){
        //我们通过str_pad()函数，把递增的$m组合成$len位的字符串，长度不够则用0来凑齐
        $numberArr[] = str_pad($i,$len,'0',STR_PAD_LEFT );
    }
    return $numberArr;
}
//字符串截取
function subText($string, $start=0, $length = null, $str = ''){
    return mb_substr($string, $start, $length, 'UTF-8').$str;
}
//随机生成字符串 $len随机生成字符串的长度
function getRandChar($len){
    $str = null;
    $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
    $max = strlen($strPol)-1;
    for($i = 0 ; $i < $len ; $i++ ){
        $str .= $strPol[rand(0,$max)];//rand($min,$max)生成介于min和max两个数之间的一个随机整数
    }
    return $str;
}
//随机生成数字
function getRandNen($len = 4) {
    $str = '';
    for ($i = 0 ; $i < $len ; $i++) {
        $str .= mt_rand(0, 9);
    }
    return $str;
    //return str_pad(mt_rand(0, 9999), $len, mt_rand(0, 9), STR_PAD_BOTH);
}
/* 获取中文的拼音声母 */
function getWordsShengMu($words){
    $fchar = ord($words{0});
    if($fchar >= ord("A") and $fchar <= ord("z") ) return strtoupper($words{0});
    $s1 = iconv("UTF-8","gb2312", $words);
    $s2 = iconv("gb2312","UTF-8", $s1);
    if($s2 == $words){$s = $s1;}else{$s = $words;}
    $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
    if($asc >= -20319 and $asc <= -20284) return "A";
    if($asc >= -20283 and $asc <= -19776) return "B";
    if($asc >= -19775 and $asc <= -19219) return "C";
    if($asc >= -19218 and $asc <= -18711) return "D";
    if($asc >= -18710 and $asc <= -18527) return "E";
    if($asc >= -18526 and $asc <= -18240) return "F";
    if($asc >= -18239 and $asc <= -17923) return "G";
    if($asc >= -17922 and $asc <= -17418) return "I";
    if($asc >= -17417 and $asc <= -16475) return "J";
    if($asc >= -16474 and $asc <= -16213) return "K";
    if($asc >= -16212 and $asc <= -15641) return "L";
    if($asc >= -15640 and $asc <= -15166) return "M";
    if($asc >= -15165 and $asc <= -14923) return "N";
    if($asc >= -14922 and $asc <= -14915) return "O";
    if($asc >= -14914 and $asc <= -14631) return "P";
    if($asc >= -14630 and $asc <= -14150) return "Q";
    if($asc >= -14149 and $asc <= -14091) return "R";
    if($asc >= -14090 and $asc <= -13319) return "S";
    if($asc >= -13318 and $asc <= -12839) return "T";
    if($asc >= -12838 and $asc <= -12557) return "W";
    if($asc >= -12556 and $asc <= -11848) return "X";
    if($asc >= -11847 and $asc <= -11056) return "Y";
    if($asc >= -11055 and $asc <= -10247) return "Z";
    return null;
}
/* 中文批量获取声母 */
function getAllShengMu($zh){
    $ret = "";
    $s1 = iconv("UTF-8","gb2312", $zh);
    $s2 = iconv("gb2312","UTF-8", $s1);
    if($s2 == $zh){$zh = $s1;}
    for($i = 0; $i < strlen($zh); $i++){
        $s1 = substr($zh,$i,1);
        $p = ord($s1);
        if($p > 160){
            $s2 = substr($zh,$i++,2);
            $ret .= $this->getWordsShengMu($s2);
        }else{
            $ret .= $s1;
        }
    }
    return $ret;
}
//获取字符串的首个拼音字母 [特殊字体无法使用]
function getFirstLetter($str){
    $firstchar_ord=ord(strtoupper($str{0}));
    if (($firstchar_ord>=65 and $firstchar_ord<=91)or($firstchar_ord>=48 and $firstchar_ord<=57)) return $str{0};
    $s=iconv("UTF-8","gb2312", $str);
    $asc=ord($s{0})*256+ord($s{1})-65536;
    if($asc>=-20319 and $asc<=-20284)return "A";
    if($asc>=-20283 and $asc<=-19776)return "B";
    if($asc>=-19775 and $asc<=-19219)return "C";
    if($asc>=-19218 and $asc<=-18711)return "D";
    if($asc>=-18710 and $asc<=-18527)return "E";
    if($asc>=-18526 and $asc<=-18240)return "F";
    if($asc>=-18239 and $asc<=-17923)return "G";
    if($asc>=-17922 and $asc<=-17418)return "H";
    if($asc>=-17417 and $asc<=-16475)return "J";
    if($asc>=-16474 and $asc<=-16213)return "K";
    if($asc>=-16212 and $asc<=-15641)return "L";
    if($asc>=-15640 and $asc<=-15166)return "M";
    if($asc>=-15165 and $asc<=-14923)return "N";
    if($asc>=-14922 and $asc<=-14915)return "O";
    if($asc>=-14914 and $asc<=-14631)return "P";
    if($asc>=-14630 and $asc<=-14150)return "Q";
    if($asc>=-14149 and $asc<=-14091)return "R";
    if($asc>=-14090 and $asc<=-13319)return "S";
    if($asc>=-13318 and $asc<=-12839)return "T";
    if($asc>=-12838 and $asc<=-12557)return "W";
    if($asc>=-12556 and $asc<=-11848)return "X";
    if($asc>=-11847 and $asc<=-11056)return "Y";
    if($asc>=-11055 and $asc<=-10247)return "Z";
    return null;
}
//过滤字符串中的中文逗号为英文逗号，并去掉前后和中间多余的逗号
function filterStrComma($str){
    if (!$str) return '';
    $str = str_replace('，',',',$str);
    return implode(',',array_filter(explode(',',$str)));
}
// ------------ 字符串函数结束 -------------------------------------------------------------- //

// ------------ 文件处理函数 -------------------------------------------------------------- //
function get_nr($url,$ref = '' ,$coo=''){
    $header = array("Referer: ".$ref."","Cookie: ".$coo);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    //----
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    //$contents = curl_exec($ch);
    ob_start();
    curl_exec($ch);
    $contents = ob_get_contents();
    ob_end_clean();
    curl_close($ch);
    return $contents;
}
// php https 获取
function get_https($url, $ref='',$coo=''){
    $header = array("Referer: ".$ref."","Cookie: ".$coo);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);//
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);//
    //----
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    //$contents = curl_exec($ch);
    ob_start();
    curl_exec($ch);
    $contents = ob_get_contents();
    ob_end_clean();
    curl_close($ch);
    return $contents;
}
//带来路的post
function post_nr_from($url, $ref, $post_data = array()){
    if (is_array ( $post_data ) && 0 < count ( $post_data )) {
        $postBodyString = "";
        foreach ( $post_data as $k => $v ) {
            $postBodyString .= "$k=" . urlencode ( $v ) . "&";
        }
    }

    $header = array("Referer: ".$ref."","Cookie: ");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    //指定post数据
    curl_setopt($ch, CURLOPT_POST, true);
    //添加变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
//有些地方post的数据要转url
function post_nr_str($url, $ref, $post_data = array()){
    if (is_array ( $post_data ) && 0 < count ( $post_data )) {
        $postBodyString = "";
        foreach ( $post_data as $k => $v ) {
            $postBodyString .= "$k=" . urlencode ( $v ) . "&";
        }
    }
    $header = array("Referer: ".$ref."","Cookie: ");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    //指定post数据
    curl_setopt($ch, CURLOPT_POST, true);
    //添加变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
//php获取curl跳转的新地址，不需要新内容
function curl_post_302($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // 获取转向后的内容
    $data = curl_exec($ch);
    $Headers = curl_getinfo($ch);
    curl_close($ch);
    if ($data != $Headers){
        return $Headers["url"];
    }else{
        return false;
    }
}
// php post
function post_nr($url, $post_data = array()){
    $postBodyString = "";
    if (is_array ( $post_data ) && 0 < count ( $post_data )) {
        foreach ( $post_data as $k => $v ) {
            $postBodyString .= "$k=" . urlencode ( $v ) . "&";
        }
        $postBodyString = substr ( $postBodyString, 0, - 1 );
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //指定post数据
    curl_setopt($ch, CURLOPT_POST, true);
    //添加变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
//php post2 json格式 用$GLOBALS["HTTP_RAW_POST_DATA"]来获取
function httpPost($url, $data){//file_get_content
    $opts = array('http' =>
        array(
            'method' => 'POST',
            'header' => 'Content-Type: application/json',
            'content' => json_encode($data)
        )
    );
    $context = stream_context_create($opts);
    $result = file_get_contents($url, false, $context);
    return $result;
}
// curl post 微信专用
function curl_post( $uri , $data )
{
    $ch = curl_init();
    curl_setopt( $ch , CURLOPT_TIMEOUT , 5 );
    curl_setopt( $ch , CURLOPT_URL , $uri );
    curl_setopt( $ch , CURLOPT_RETURNTRANSFER , TRUE );
    curl_setopt( $ch , CURLOPT_SSL_VERIFYPEER , 0 );
    curl_setopt( $ch , CURLOPT_SSL_VERIFYHOST , 0 );
    //指定post数据
    curl_setopt( $ch , CURLOPT_POST , TRUE );
    //添加变量
    curl_setopt( $ch , CURLOPT_POSTFIELDS , $data );
    $output = curl_exec( $ch );
    curl_close( $ch );

    return $output;
}
//curl远程执行函数
function curl($url, $postFields = null)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FAILONERROR, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //https 请求
    if(strlen($url) > 5 && strtolower(substr($url,0,5)) == 'https' )
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    }

    if (is_array($postFields) && 0 < count($postFields))
    {
        $postBodyString = '';
        $postMultipart = false;
        foreach ($postFields as $k => $v)
        {
            if('@' != substr($v, 0, 1))//判断是不是文件上传
            {
                $postBodyString .= "$k=".urlencode($v)."&";
            }else{
                //文件上传用multipart/form-data，否则用www-form-urlencoded
                $postMultipart = true;
            }
        }
        $postFields = trim($postBodyString,'&');
        unset($k, $v);
        curl_setopt($ch, CURLOPT_POST, true);
        if ($postMultipart)
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        }else{
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        }
    }
    /*try {
        $reponse = curl_exec($ch);
        if (curl_errno($ch))
        {
            throw new Exception(curl_error($ch),0);
        }else{
            $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode)
            {
                throw new Exception('httpStatusError',$httpStatusCode);
            }
        }
    }catch (Exception $e){
        $reponse = null;
    }*/
    $reponse = curl_exec($ch);
    curl_close($ch);
    return json_decode($reponse,true);
}
//自动保存文件函数
function fileSave($path,$content) {
    $oldContent = '';
    if(!file_exists($path)) {
        file_put_contents($path,'');
    } else {
        $oldContent = file_get_contents($path);
    }
    $newContent = $oldContent ."\r\n". $content;
    file_put_contents($path, $newContent);
}
//创建多级文件夹
function creatDir($path){
    if(!is_dir($path)) {
        if(file::creatdir(dirname($path))){
            mkdir($path,0755);
            return true;
        }
        return false;
    }else{
        return true;
    }
}
//生成缩略图 文件名带_min [gd库]
function resizeImageGD($pic_url1, $newSize = 265, $newFileName = '') {
    $fileName = basename($pic_url1);
    $savePath = dirname($pic_url1);
    //生成的小图文件名
    $resizeFileName1 = $newFileName ? $newFileName : str_replace(".jpg", "_min.jpg", $fileName);
    //获取原图内容
    $src = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"].$pic_url1);
    //取得源图片的宽度和高度
    $size_src = getimagesize($_SERVER["DOCUMENT_ROOT"].$pic_url1);
    $old_width = $size_src['0'];
    $old_height = $size_src['1'];
    //指定缩放出来的最大的宽度（也有可能是高度）
    //计算等比尺寸
    if($old_width > $old_height){
        $new_width1 = $newSize;
        $new_height1 = $old_height * ($newSize / $old_width);
    } else {
        $new_height1 = $newSize;
        $new_width1 = $old_width * ($newSize / $old_height);
    }
    //声明一个$w宽，$h高的真彩图片资源
    if(function_exists("imagecopyresampled")) {
        //生成第1张图片
        $image_1 = imagecreatetruecolor($new_width1, $new_height1);
        //2.上色
        $color=imagecolorallocate($image_1, 255,255,255);
        //3.设置透明
        imagecolortransparent($image_1, $color);
        imagefill($image_1,0,0,$color);
        imagecopyresampled($image_1, $src, 0,0,0,0, $new_width1, $new_height1, $old_width, $old_height);
    } else {
        //生成第1张图片
        $image_1 = imagecreate($new_width1, $new_height1);
        //2.上色
        $color=imagecolorallocate($image_1, 255,255,255);
        //3.设置透明
        imagecolortransparent($image_1, $color);
        imagefill($image_1,0,0,$color);
        imagecopyresized($image_1, $src,0,0,0,0, $new_width1, $new_height1, $old_width, $old_height);
    }
    imagejpeg($image_1, $_SERVER["DOCUMENT_ROOT"].$savePath."/".$resizeFileName1); //保存第1张图片
}
//生成缩略图 文件名带_min [imagick]
function resizeImage($pic_url1, $newSize = 265, $newFileName = '') {
    $fileName = basename($pic_url1);
    $savePath = dirname($pic_url1);
    //生成的小图文件名
    $resizeFileName1 = $newFileName ? $newFileName : str_replace(".jpg", "_min.jpg", $fileName);
    if(!file_exists(rtrim(root, '/').$pic_url1)) return; //文件不存在要退出 不然页面会停止
    //取得源图片的宽度和高度
    $size_src = getimagesize(rtrim(root, '/').$pic_url1);
    $old_width = $size_src['0'];
    $old_height = $size_src['1'];
    //指定缩放出来的最大的宽度（也有可能是高度）
    //计算等比尺寸 只有超出最大尺寸才压缩
    if($old_width > $newSize || $old_height > $newSize) {
        if($old_width >= $old_height){
            $new_width1 = $newSize;
        } else {
            $new_width1 = $old_width * ($newSize / $old_height);
        }
        if (class_exists('imagick')) {
            $im = new imagick(rtrim(root, '/').$pic_url1);
            $im->thumbnailImage($new_width1, 0);
            $im->writeImage(rtrim(root, '/'). $savePath."/".$resizeFileName1);
        }
    } else {//图片太小 也要复制成小图
        if (class_exists('imagick')) {
            $im = new imagick(rtrim(root, '/').$pic_url1);
            $im->thumbnailImage($old_width, 0);
            $im->writeImage(rtrim(root, '/'). $savePath."/".$resizeFileName1);
        }
    }
}
// ---------------------------- 文件函数结束 ------------------------------ //

// ---------------------------- 支付 开始 ------------------------------ //
/**
 * 验证AppStore内付
 * @param  string $receipt_data 付款后凭证
 * @return array                验证是否成功
 */
function validate_apple_pay($receipt_data){
    // 验证参数
    if (strlen($receipt_data)<20){
        return ['status'=>false,'message'=>'非法参数'];
    }
    // 请求验证
    $html = acurl($receipt_data);
    $data = json_decode($html,true);
    // 如果是沙盒数据 则验证沙盒模式
    if($data['status'] == '21007'){
        // 请求验证
        $html = acurl($receipt_data, 1);
        $data = json_decode($html,true);
        $data['sandbox'] = '1';
    }
    if (isset($_GET['debug'])) {
        //exit(json_encode($data));
        return $data;
    }
    // 判断是否购买成功
    $result = intval($data['status']) === 0 ? ['status'=>true,'message'=>'购买成功'] : ['status'=>false,'message'=>'购买失败 status:'.$data['status']];
    return $result;
}

/**
 * 21000 App Store不能读取你提供的JSON对象
 * 21002 receipt-data域的数据有问题
 * 21003 receipt无法通过验证
 * 21004 提供的shared secret不匹配你账号中的shared secret
 * 21005 receipt服务器当前不可用
 * 21006 receipt合法，但是订阅已过期。服务器接收到这个状态码时，receipt数据仍然会解码并一起发送
 * 21007 receipt是Sandbox receipt，但却发送至生产系统的验证服务
 * 21008 receipt是生产receipt，但却发送至Sandbox环境的验证服务
 */
function acurl($receipt_data, $sandbox=0){
    //小票信息
    $POSTFIELDS = array("receipt-data" => $receipt_data);
    $POSTFIELDS = json_encode($POSTFIELDS);
    //正式购买地址 沙盒购买地址
    $url_buy     = "https://buy.itunes.apple.com/verifyReceipt";//先验证
    $url_sandbox = "https://sandbox.itunes.apple.com/verifyReceipt";//后验证
    $url = $sandbox ? $url_sandbox : $url_buy;
    //简单的curl
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $POSTFIELDS);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

// ---------------------------- 支付 结束 ------------------------------ //

/**
 * 银行卡号正则
 */
function checkBank($no){
    //$no = '6228480402564890018';
    $arr_no = str_split($no);
    $last_n = $arr_no[count($arr_no)-1];
    krsort($arr_no);
    $i = 1;
    $total = 0;
    foreach ($arr_no as $n){
        if($i%2==0){
            $ix = $n*2;
            if($ix>=10){
                $nx = 1 + ($ix % 10);
                $total += $nx;
            }else{
                $total += $ix;
            }
        }else{
            $total += $n;
        }
        $i++;
    }
    $total -= $last_n;
    $total *= 9;
    if($last_n == ($total%10)){
        return true;
    }else{
        return false;
    }
}




/*
 * PHP 计算两个时间戳之间相差的时间
 * 功能：计算两个时间戳之间相差的日时分秒
 * $begin_time  开始时间戳
 * $end_time 结束时间戳
*/
function timeDiff($begin_time,$end_time)
{
    if($begin_time < $end_time){
        $starttime = $begin_time;
        $endtime = $end_time;
    }else{
        $starttime = $end_time;
        $endtime = $begin_time;
    }
    //计算天数
    $timediff = $endtime-$starttime;
    $days = intval($timediff/86400);
    //计算小时数
    $remain = $timediff%86400;
    $hours = intval($remain/3600);
    //计算分钟数
    $remain = $remain%3600;
    $mins = intval($remain/60);
    //计算秒数
    $secs = $remain%60;
    $res = array("day" => $days,"hour" => $hours,"min" => $mins,"sec" => $secs);
    return $res;
}

?>
