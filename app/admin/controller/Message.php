<?php
namespace app\admin\controller;

Class Message extends Base
{
	public $table = '';
	public function initialize()
	{
		parent::initialize();
		$this->table = controller('common/message');
        $this->userRole = session('adminInfo.userRole');
	}
	public function messageList()
	{
		$dotype = input('dotype','');
		$fromtime = input('fromtime','');
        $endtime = input('endtime','');
        $username = input('username','');
        $disname = input('disname','');
        $mor ='1=1';
        if( in_array($this->userDisType, [1,2,3]) ){
            $mor = 'm_dis_id='.$this->userDisId;
        }
        $where = $mor.' and m_status = 1';
        if ($dotype) $where = $where.' AND m_type='.$dotype;
        if ($fromtime) $where = $where.' AND m_addtime >='.$fromtime;
        if ($endtime) $where = $where.' AND m_addtime <='.$endtime;
        if ($username) $where = $where.' AND m_uname like "%'.$username.'%"';
        if ($disname) $where = $where.' AND m_dis_name like "%'.$disname.'%"';
        $qurest = ['dotype'=>$dotype,'fromtime'=>$fromtime,'endtime'=>$endtime,'username'=>$username,'disname'=>$disname];
        $resultList = $this->table->getPageList($where,'*','m_addtime DESC',20,$qurest,function ($e) {
                return $e;
            });
        $page = $resultList->render();
        $resultList = $resultList->isEmpty() ? '' : $resultList;

        $this->assign('username',$username);
        $this->assign('disname',$disname);
        $this->assign('fromtime',$fromtime);
        $this->assign('endtime',$endtime);
        $this->assign('resultList',$resultList);
        $this->assign('page',$page);
        $this->assign('dotype',$dotype);
		return $this->fetch();
	}
}