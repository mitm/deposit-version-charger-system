<?php
namespace app\admin\controller;

use app\admin\controller\Base;
use think\facade\Cache;
class Index extends Base
{
    public function initialize()
    {
        parent::initialize();
    }
    //后台首页
    public function index()
    {
        $where = 'm_affiliation=1 AND m_type=1 AND m_status=1';
        //如果不是超级管理员，则判断访问权限来显示左边菜单
        if ($this->userId != 1) {
            $r_id = controller('common/user')->getUserInfoByField($this->userId,'u_role');//获取用户角色ID
            $m_ids = controller('common/role')->getRoleInfoByField('r_status != -1 AND r_id='.$r_id,'r_mid');
            if (!$m_ids) {
                if (session('?adminInfo')) session('adminInfo',null);
                $newData = [
                    'user_id' => $this->userId,
                    'user_role_id' => $r_id,
                    'role_mid' => $m_ids,
                    'user_name' => $this->userName ? $this->userName : $this->userNick,
                    'user_phone' => $this->userPhone,
                    'url' => request()->module().'/'.request()->controller().'/'.request()->action(),
                    'addtime' => myTime(),
                ];
                db('no_power_log')->insert($newData);
                $this->error($this->userName.$this->userPhone.$this->userNick.'没权限！请联系超级管理员！');
                exit;
            }
            $where = $where.' AND m_id IN('.$m_ids.')';
        }

        $menuCom = controller('common/menu');
        $fields = 'm_id,m_title,m_url';

        $menuList = $menuCom->getMenuList($fields, 'm_parent_id=0 AND '.$where, $where);
           // var_dump($menuList[3]["m_child_menu"][2] ["m_child_menu"]);die;
        if ($this->userDisType==1){//一级菜单
            unset($menuList[3]["m_child_menu"][2]["m_child_menu"][0]);

        }
        
        if ($this->userDisType==2){//二级菜单
            unset($menuList[3]["m_child_menu"][2] ["m_child_menu"][0]);
            unset($menuList[3]["m_child_menu"][3]["m_child_menu"][1]);
        }
      
      if ($this->userDisType==3){//二级菜单
            unset($menuList[3]["m_child_menu"][0]["m_child_menu"][0]);
            unset($menuList[3]["m_child_menu"][0]["m_child_menu"][1]);
        }
      
      
        $this->assign("menuList",$menuList);
        $this->assign("userName",$this->userName);
        $this->assign("userId",$this->userId);
		$this->assign('homeUrl', request()->domain());
		return $this->fetch();
	}
	//后台欢迎页面
	public function welcome()
	{
        $disId = input('disId',0);
        $where  = 'o_line_number > 0';
        $equipmentWhere = 'd_status != -1';
        if( in_array($this->userDisType, [1,2,3]) || $disId) {
            $disId = $disId ? $disId : $this->userDisId;
            //统计所有设备，已分配
            $equipmentAllTotalCount = db('distributor_equipments')->where('d_status!=-1 AND (d_dis_id='.$disId.' or d_dis_id1='.$disId.' or d_dis_id2='.$disId.')')->count();//新设备已分配的数量
        } else {
            //统计所有设备，已分配
            $equipmentAllTotalCount = db('equipments')->where('e_status=2')->count();//新设备已分配的数量
        }
        $where = $where.' AND o_dis_id='.$disId;

        /* -------------------------------------------------------------------
           总体统计，含下属的 今日订单总金额、累计总金额、设备数量、订单数量、实际收益
        ----------------------------------------------------------------------*/
        if ( $disId) {
            //获取属下分销商索引ID
            $disIdData = [];
            $oneDistributor = db('distributor')->where('d_parent_id='.$disId.' AND d_status=1')->column('d_id');//一层属下
            if ($oneDistributor) {
                $disIdData = $oneDistributor;
                foreach ($oneDistributor as $vDisId) {
                    //二层属下
                    if ($towDistributor = db('distributor')->where('d_parent_id='.$vDisId.' AND d_status=1')->column('d_id')) {
                        $disIdData = array_merge($disIdData,$towDistributor);
                        foreach ($towDistributor as $vTowDisId) {
                            if ($thirdDistributor = db('distributor')->where('d_parent_id='.$vTowDisId.' AND d_status=1')->column('d_id')) {
                                $disIdData = array_merge($disIdData,$thirdDistributor);
                            };//三层属下
                        }
                    }
                }
                $disIdData = array_unique($disIdData);
            }
            //处理属下分销商索引ID，并加上自己的分销商索引ID
            if (!empty($disIdData)) {
                $disIdData = implode(',',$disIdData);
                $disIdData = $disIdData ? $disIdData.','.$disId : $disId;
            }
            $allWhere = $disIdData ? '  o_isrefund =1  and o_line_number > 0 AND o_dis_id IN ('.$disIdData.')' : $where;
        } else {
            $allWhere = '  o_isrefund =1  and o_line_number > 0';
        }

        $allData = controller('order')->countTotal($allWhere,'op_way!=2 AND dis_id='.$disId,$equipmentWhere,$disId);
        $allData['equipmentAllTotalCount'] = $equipmentAllTotalCount;

        $starTime = strtotime(date('Y-m-d',time()).' 12:00:00');
        $endTime = strtotime(date('Y-m-d',strtotime("1 day")).' 11:59:59');
        if (date('Y-m-d H:i:s',time()) < date('Y-m-d',time()).' 12:00:00') {
            $starTime = strtotime(date('Y-m-d',strtotime("-1 day")).' 12:00:00');
            $endTime = strtotime(date('Y-m-d',time()).' 11:59:59');
        }

        $resultList =db('order_pay')->field('sum(o_price) m,o_dis_name')->where('  o_paytime >= "'.$starTime.'" AND o_paytime <= "'.$endTime.'"')->group("o_dis_id")->order("m DESC")->select();

        $this->assign('resultList',$resultList);

        $resultListAll =db('order_pay')->field('sum(o_price) m,o_dis_name')->where("pay_status =1 ")->group("o_dis_id")->order("m DESC")->select();
        $withdraw=null;

        if( $this->userId==1) {
            
            $qitian = \Db::query("SELECT  count(*) num,sum(o_price) money, DATE_FORMAT(FROM_UNIXTIME(o_paytime),'%m-%d') time FROM `de_order_pay` WHERE `status` =1  GROUP BY  DATE_FORMAT(FROM_UNIXTIME(o_paytime),'%Y-%m-%d') ORDER BY o_id desc LIMIT 0,6");
            
            $shouru = \Db::query("SELECT * FROM `de_distributor_account` LEFT JOIN de_distributor on dis_id =d_id  ORDER BY income desc LIMIT 0,6");
            $withdraw =  \Db::query("SELECT * FROM `de_withdraw`  left JOIN de_distributor  on dis_id = d_id ORDER BY `create_time` DESC LIMIT 0,10");
                  
         $this->assign('qitian',$qitian);
         $this->assign('shouru',$shouru);

        } 
        

         $this->assign('withdraw',$withdraw);




        $this->assign('resultListAll',$resultListAll);

        $this->assign("userId",$this->userId);
        $this->assign('allData',$allData);
        return $this->fetch();
    }
    //清空缓存文件
	public function clearCache(){
        return Cache::clear() ? reAjaxMsg(1,'缓存清除成功！') : reAjaxMsg(0,'缓存清除失败！');
	}
	//设置帮助中心文章页面
    public function setHelpConfig()
    {
        $this->assign('info',db('config')->where('f_id=1')->value('f_help_content'));
        $this->assign('gotoSetInfoUrl',url('index/gotoSetHelpConfig'));
        return $this->fetch();
    }
    //设置帮助中心文章功能
    public function gotoSetHelpConfig()
    {
        $content = input('editorValue','');
        if (!$content){
            return reAjaxMsg(0,'请输入文章内容！');
        }
        return db('config')->where('f_id=1')->update(['f_help_content'=>$content]) ? reAjaxMsg(1,'设置成功！') : reAjaxMsg(0,'设置失败！');
    }
    //恢复分销商账号
    public function returnDis1111(){
        //获取所有分销商数据
        $oneData = db('distributor')->select();
        $newOne1 = [];
        foreach ($oneData as $vone) {
            if ($vone['d_user_id']) {
                if (!db('user')->where('u_type=2 AND (u_id = '.$vone['d_user_id'].' OR u_disuid='.$vone['d_user_id'].')')->find()) {
                    $newOne1[] = $vone;
                }
            } else {
                if (!db('user')->where('u_type=2 AND u_disid = '.$vone['d_id'])->find()) {
                    $newOne1[] = $vone;
                }
            }
        }
        echo '<br/>';
        echo '<br/>';
        print_r(count($oneData));
        echo '<br/>';
        print_r(count($newOne1));
        echo '<br/>';
        print_r(implode(',',array_column($newOne1,'d_id')));
        if (!empty($newOne1)) {
            foreach ($newOne1 as $value) {
                if ($info = db('user')->where('u_phone="'.$value['d_principal_phone'].'"')->find()) {
                    /*if ($info['u_disuid']) continue;
                    db('user')->where('u_phone="'.$value['d_principal_phone'].'"')->update(['u_phone'=>null,'u_password'=>null]);
                    $newData = [
                        'u_name' => $value['d_principal_phone'],
                        'u_phone' => $value['d_principal_phone'],
                        'u_password' => $info['u_password'] ? $info['u_password'] : pswCAM($value['d_principal_phone'],123456),
                        'u_role' => $info['u_role'],
                        'u_addtime' => myTime()
                    ];
                    $userId = db('user')->insertGetId($newData);
                    db('user')->where('u_phone="'.$value['d_principal_phone'].'"')->update(['u_disid'=>$value['d_id'],'u_disuid'=>$userId]);
                    db('distributor')->where('d_id='.$value['d_id'])->update(['d_user_id'=>$userId,'d_user_name'=>$value['d_principal_phone'],'d_user_phone'=>$value['d_principal_phone']]);*/
                } else {
                    $newData = [
                        'u_name' => $value['d_principal_phone'],
                        'u_phone' => $value['d_principal_phone'],
                        'u_password' => pswCAM($value['d_principal_phone'],123456),
                        'u_role' => $info['u_role'],
                        'u_addtime' => myTime(),
                        'u_type' => 2,
                        'u_status' => 1
                    ];
                    if ($userId = db('user')->insertGetId($newData)) {
                        db('distributor')->where('d_id='.$value['d_id'])->update(['d_user_id'=>$userId,'d_user_name'=>$value['d_principal_phone'],'d_user_phone'=>$value['d_principal_phone']]);
                    }
                }
            }
        }

        echo '<br/>';
        echo '<br/>';
        //print_r($oneData);
        print_r(count($oneData));
        echo '<br/>';
        print_r(count($newOne1));
        echo '<br/>';
        //print_r($newOne1);
        print_r(implode(',',array_column($newOne1,'d_id')));
        //获取没有被用户绑定的分销商数据
        //（1）根据分销商中用户信息来判断是否被用户绑定
        //     A.d_user_id != u_id
        //     B.d_user_id != u_disuid || d_id != u_disid
        //（2）如果分销商的用户信息为空时，判断用户是否绑定分销商ID
        //     A.d_id != u_disid
    }

    /*public function dedede(){
        $oneData = db('distributor')->where('d_id in(41,104,111,118,122,125,134,140,141,142,145,149,161,167,168,180,184,192,193,207,215,218,219,220,233,241,243,246,250,253,263)')->select();
        foreach ($oneData as $v) {
            $role = $v['d_type'] == 1 ? 4 : ($v['d_type'] == 2 ? 5 : 6);
            db('user')->where('u_id='.$v['d_user_id'])->update(['u_role'=>$role]);
        }
        //print_r($oneData);
    }*/
    //获取绑定分销商的微信端，新增账户并将新增绑定到分销商
    public function dede1()
    {
        /*$userList = db('user')->where('openid is not null AND u_disuid is not null')->field('u_id,u_disuid')->select();
        $arrData = $withdrawData1 = $withdrawAccountData = $distributorData = $distributorAccountData = $priceData = [];
        if ($userList) {
            foreach ($userList as $v) {
                $u_id = $v['u_id'];
                if ($u_id == $v['u_disuid'] && $disInfo = db('distributor')->where('d_user_id='.$u_id)->find()) {
                    $d_id = $disInfo['d_id'];
                    //提现
                    if ($withdrawInfo = db('withdraw')->where('dis_id='.$d_id.' AND user_id='.$u_id)->find()) $withdrawData1[] = $withdrawInfo;
                    //提现基本信息
                    if ($withdrawAccountInfo = db('withdraw_account')->where('user_id='.$u_id)->find()) $withdrawAccountData[] = $withdrawAccountInfo;
                }
            }
        }
        session('userList_de',$userList);
        session('withdrawInfo_de',$withdrawData1);
        session('withdrawAccountInfo_de',$withdrawAccountData);*/
    }
    public function dede2()
    {
        /*$arrData = $withdrawData1 = $withdrawAccountData = $distributorData = $distributorAccountData = $priceData = [];
        if ($userList = session('userList_de')) {
            foreach ($userList as $v) {
                $u_id = $v['u_id'];
                if ($u_id == $v['u_disuid'] && $disInfo = db('distributor')->where('d_user_id='.$u_id)->find()) {
                    $d_id = $disInfo['d_id'];
                    //广告
                    if ($bannerInfo = db('distributor_banner')->where('b_dis_id='.$d_id.' AND b_uid='.$u_id)->find()) $distributorData[] = $bannerInfo;
                    //账户
                    if ($accountInfo = db('distributor_account')->where('dis_id='.$d_id.' AND user_id='.$u_id)->find()) $distributorAccountData[] = $accountInfo;
                    //分销商价格设置表
                    if ($priceInfo = db('distributor_price')->where('p_dis_id='.$d_id.' AND p_dis_uid='.$u_id)->find()) $priceData[] = $priceInfo;
                }
            }
        }
        session('bannerInfo_de',$distributorData);
        session('accountInfo_de',$distributorAccountData);
        session('priceInfo_de',$priceData);*/
    }
    public function dede3()
    {
        /*$userList = session('userList_de');
        foreach ($userList as $v) {
            $u_id = $v['u_id'];
            if ($u_id == $v['u_disuid'] && $disInfo = db('distributor')->where('d_user_id='.$u_id)->find()) {
                $userId = model('user')->insertGetId()
                $d_id = $disInfo['d_id'];
                //提现
                model('withdraw')->save(['user_id'=>],'dis_id='.$d_id.' AND user_id='.$u_id);
                //提现基本信息
                if ($withdrawAccountInfo = db('withdraw_account')->where('user_id='.$u_id)->find()) $withdrawAccountData[] = $withdrawAccountInfo;
                //广告
                if ($bannerInfo = db('distributor_banner')->where('b_dis_id='.$d_id.' AND b_uid='.$u_id)->find()) $distributorData[] = $bannerInfo;
                //账户
                if ($accountInfo = db('distributor_account')->where('dis_id='.$d_id.' AND user_id='.$u_id)->find()) $distributorAccountData[] = $accountInfo;
                //分销商价格设置表
                if ($priceInfo = db('distributor_price')->where('p_dis_id='.$d_id.' AND p_dis_uid='.$u_id)->find()) $priceData[] = $priceInfo;
            }
        }

        $withdrawData1 = session('withdrawInfo_de');
        $withdrawAccountData = session('withdrawAccountInfo_de');
        $distributorData = session('bannerInfo_de');
        $distributorAccountData = session('accountInfo_de');
        $priceData = session('priceInfo_de');
        print_r($withdrawData1);
        echo '<br/>';
        echo '<br/>';
        print_r($withdrawAccountData);
        echo '<br/>';
        echo '<br/>';
        print_r($distributorData);
        echo '<br/>';
        echo '<br/>';
        print_r($distributorAccountData);
        echo '<br/>';
        echo '<br/>';
        print_r($priceData);
        echo '<br/>';
        echo '<br/>';
        print_r($userList);*/
        die;
        $arrData = $withdrawData1 = $withdrawAccountData = $distributorData = $distributorAccountData = $priceData = [];
        if ($userList = session('userList_de')) {
            foreach ($userList as $v) {
                $u_id = $v['u_id'];
                if ($u_id == $v['u_disuid'] && $disInfo = db('distributor')->where('d_user_id='.$u_id)->find()) {
                    $d_id = $disInfo['d_id'];
                    //提现
                    if ($withdrawInfo = db('withdraw')->where('dis_id='.$d_id.' AND user_id='.$u_id)->find()) $withdrawData1[] = $withdrawInfo;
                    //提现基本信息
                    if ($withdrawAccountInfo = db('withdraw_account')->where('user_id='.$u_id)->find()) $withdrawAccountData[] = $withdrawAccountInfo;
                    //广告
                    if ($bannerInfo = db('distributor_banner')->where('b_dis_id='.$d_id.' AND b_uid='.$u_id)->find()) $distributorData[] = $bannerInfo;
                    //账户
                    if ($accountInfo = db('distributor_account')->where('dis_id='.$d_id.' AND user_id='.$u_id)->find()) $distributorAccountData[] = $accountInfo;
                    //分销商价格设置表
                    if ($priceInfo = db('distributor_price')->where('p_dis_id='.$d_id.' AND p_dis_uid='.$u_id)->find()) $priceData[] = $priceInfo;
                }
            }
        }
        print_r(count($arrData));
        /*print_r($withdrawData1);
        print_r($withdrawAccountData);*/
        print_r($priceData);
    }
}
