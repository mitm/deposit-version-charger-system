<?php
namespace app\admin\controller;

class Role extends Base
{
	
	public function initialize()
	{
        parent::initialize();
	}



    /*
     * ===================================================================================
     *
     * 页面部分
     *
     * ===================================================================================
     */

    //角色列表
    public function roleList(){
        $roleList = controller('common/role')->getRoleList('r_status!=-1');
        $this->assign("roleList",$roleList);
        $this->assign("setRoleUrl",'role/setRole');
        $this->assign("gotoSetRoleUrl",'role/gotoSetRole');
        return $this->fetch();
    }
    //设置角色
    public function setRole() {
        $rid = input('r_id',0);
        $roleInfo = [
            'r_name'=>'',
            'r_status'=>'',
            'r_id'=>''
        ];
        if ($rid) {
            $roleList = controller('common/role')->getRoleInfo('r_id='.$rid);
            $roleInfo = $roleList ? $roleList : $roleInfo;
        }
        $this->assign("roleInfo",$roleInfo);
        $this->assign("gotoSetRoleUrl",'role/gotoSetRole');
        return $this->fetch();
    }
    //设置角色权限
    public function setRolePower() {
        $r_id = input('r_id',1);//默认普通会员
        $menuCom = controller('common/menu');
        $fields = 'm_id,m_title,m_url,m_status,m_type,m_affiliation,m_addname,m_addtime';
        $menuList = $menuCom->getMenuPage($fields,'m_parent_id=0', '', 100);// 获取分页显示
        $page = $menuList->render();
        $menuList = $menuList->isEmpty() ? '' : $menuList;

        $roleList = controller('common/role')->getRoleList('r_status != -1');
        $roleInfo = controller('common/role')->getRoleInfo('r_status != -1 AND r_id='.$r_id,'r_mid');
        $this->assign("r_mid",empty($roleInfo['r_mid']) ? array() : explode(',',$roleInfo['r_mid']));
        $this->assign("roleList",$roleList);
        $this->assign("r_id",$r_id);
        $this->assign("menuList",$menuList);
        $this->assign("gotoSetRolePowerUrl",'role/setRolePower');
        $this->assign('page', $page);
        return $this->fetch();
    }




    /*
     * ===================================================================================
     *
     * 功能部分
     *
     * ===================================================================================
     */

    //设置角色
    public function gotoSetRole() {
        $r_id = input('post.r_id',0);//角色ID
        $r_name = input('post.r_name',0);//角色名称
        $r_status = input('post.r_status',0);//角色状态
        if (!$r_name && $r_status != -1) {
            return reAjaxMsg(0,'角色名称不能为空！');
        }
        $newData = array(
            'r_status' => $r_status,
        );
        if ($r_status != -1) {//非删除
            $newData['r_name'] = $r_name;
        }
        if ($r_id) {//编辑、删除
            $res = controller('common/role')->editRole('r_id='.$r_id,$newData);
        } else {//添加
            $newData['r_addtime'] = myTime();
            $newData['r_adduid'] = $this->userId;
            $res = controller('common/role')->addRole($newData);
        }
        $msgStr = $r_status == -1 ? '删除' : ($r_id ? '编辑' : '添加');
        if ($res == 'ok') {
            return reAjaxMsg(1,$msgStr.'成功！');
        }
        return reAjaxMsg(0,$msgStr.'失败！'.$res);
    }
    //设置角色权限
    public function gotoSetRolePower() {
        $r_id = input('post.r_id',0);//角色ID
        $m_ids = input('post.mids',0);//菜单ID
        if (!$r_id) {
            return reAjaxMsg(0,'请选择角色！');
        }
        if (empty($m_ids)) {
            return reAjaxMsg(0,'请选择角色模块！');
        }
        $res = controller('common/role')->editRole('r_id='.$r_id,['r_mid'=>$m_ids]);
        if ($res == 'ok') {
            return reAjaxMsg(1,'角色权限提交成功！');
        }
        return reAjaxMsg(0,'角色权限提交失败！');

    }
}