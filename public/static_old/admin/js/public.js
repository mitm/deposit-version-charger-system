
//初始化layui插件
if ( typeof layui != "undefined") {
    layui.use(['element','form','layer'], function(){
        var layer = layui.layer;
    });
    /*;!function(){
        var layer = layui.layer;
        var element = layui.element;
    }();*/
}

//iframe自适应高度
function reinitIframe(){
    var iframe = document.getElementById("main_box");
    try{
        var bHeight = iframe.contentWindow.document.body.scrollHeight;
        var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
        iframe.height = Math.max(bHeight, dHeight);
        iframe.width = '100%';
    }catch (ex){}
}
window.setInterval("reinitIframe()", 200);

//分页设置
function pageFun() {
    //样式
    var pagination = $('.pagination');
    pagination.find('.active').addClass('layui-laypage-em layui-disabled');
    pagination.find('li').addClass('pagelis');
    pagination.find('li:first').addClass('layui-laypage-prev page_first_last').find('span').html('上一页');
    pagination.find('li:first').find('a').html('上一页');
    pagination.find('li:last').addClass('layui-laypage-prev page_first_last page_last_right').find('a').html('下一页');
    pagination.find('li:last').find('span').html('下一页');
}
//最大化打开新窗口
function openPage(url,title){
    layer.full(layer.open({
        type: 2 //此处以iframe举例
        ,title: title
        ,area: ['600px','100%']
        ,shade: 0
        ,content: url
        ,scrollbar: true
        ,maxmin: true
        ,yes: function(){
            $(that).click();
        }
        ,success: function(layero,index){
            layer.setTop(layero); //重点2
        }
    }));
}
//管理员退出
function logout(url){
    layer.open({
        type: 1
        ,title: false //不显示标题栏
        ,closeBtn: false
        ,area: '300px;'
        ,shade: 0.8
        ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
        ,btn: ['安全退出', '取消']
        ,btnAlign: 'c'
        ,moveType: 1 //拖拽模式，0或者1
        ,content: '<div style="padding: 30px;line-height: 30px;text-align: center;">亲，您确认退出登录吗？</div>'
        ,success: function(layero){
          
        },
        yes: function() {
            $.post(url,{},function(data){
                layer.msg(data.msg,{time:2000},function(){
                    parent.location.reload();
                })
            });
        }
    });
}

$(function(){
    //分页设置
    pageFun();
});
