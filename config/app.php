<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用名称
    'app_name'               => '按摩枕',
    // 应用地址
    'app_host'               => 'www.lubanit.com',
    // 应用调试模式
    'app_debug'              => true,
    // 应用Trace
    'app_trace'              => false,
    // 应用模式状态
    'app_status'             => '',
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'Asia/Shanghai',
    // 是否开启多语言
    'lang_switch_on'         => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => '',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'index',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空模块名
    'empty_module'           => '',
    // 默认的空控制器名
    'empty_controller'       => 'Error',
    // 操作方法前缀
    'use_action_prefix'      => false,
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // HTTPS代理标识
    'https_agent_name'       => '',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由延迟解析
    'url_lazy_route'         => false,
    // 是否强制使用路由
    'url_route_must'         => false,
    // 合并路由规则
    'route_rule_merge'       => false,
    // 路由是否完全匹配
    'route_complete_match'   => false,
    // 使用注解路由
    'route_annotation'       => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // 全局请求缓存排除规则
    'request_cache_except'   => [],

    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => Env::get('think_path') . 'tpl/dispatch_jump.tpl',
    'dispatch_error_tmpl'    => Env::get('think_path') . 'tpl/dispatch_jump.tpl',

    // 异常页面的模板文件
    'exception_tmpl'         => Env::get('think_path') . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',
    //平台路径
    'system_url'             => 'http://www.lubanit.com',
    'qrcode_url'             => 'http://www.lubanit.com/mobile.php/index/index/?e_number=',
    'upload_path'            => 'http://www.lubanit.com',
    'wxHfPay'                => [
        'app_id'     => 'wx91727038b637e581',/*微信支付的APPID*/
        'api_key'    => 'www.lubanit.com',/*在微信商户平台上自己设定的api密钥 32位*/
        'mch_id'     => '1531976101',/*微信申请成功之后邮件中的商户id*/
        'notify_url' => 'http://www.lubanit.com/api.php/Paycallback/wxNotifyUrlOfHf',
    ],
    'wxJsapiPay'             => [
        'AppID'           => 'wx91727038b637e581',
        'AppSecret'       => 'www.lubanit.com',
        //   'AppKey' => 'DaMengDaLv2016040109062019901130',//公众号APPKey 企业付款到零钱需要
        'ApiKey'          => 'www.lubanit.com',
        'MchId'           => '1531976101',
        'NotifyUrl'       => 'http://www.lubanit.com/api.php/Paycallback/wxNotifyUrlOfHf',
        'NotifyUrlYajin'       => 'http://www.lubanit.com/api.php/Paycallback/wxNotifyUrlOfHfYajin',
        'CertificatePath' => Env::get('root_path') . 'upload',//证书路径 企业付款到零钱需要
    ],
    'wxGongzhonghao'         => [
        'AppID'     => 'wx91727038b637e581',
        'AppSecret' => 'www.lubanit.com'
    ],
    'wxMiniPay'             => [
        'AppID'           => 'wx8790bd1628f23628',
        'AppSecret'       => 'www.lubanit.com',
        'ApiKey'          => 'www.lubanit.com',//公众号APPKey 企业付款到零钱需要
        'MchId'           => '1531976101',
        'NotifyUrl'       => 'http://www.lubanit.com/api.php/Paycallback/wxMiniNotifyUrlOfHf',
    ],
    'alipayApp'              => [
        //应用ID,您的APPID。
        'app_id'               => "2021002117684078",
        //商户私钥，您的原始格式私钥,一行字符串
        'merchant_private_key' => "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCtHS3yBZn8sp7aKURj2E+VbwLsqWgVZiVojQ0W4Ul/qIFay54ZE5uOWBMqPITMZqh00IwvmwtRv6wzq/DXanLT482aUPLThx//AcCQARDeUeabW2folHuvg6vDbViC7+a5CfGjk6lkXdOj7l6MS7UJ6irt7fb2d6qkObS6/IyQRBvHXS2zPLUKHvejpWSFSs9lRValdaCfsBLgTsQHu3EiOMcer25YFYMNBOTo2E+WkTWI/mivb+AveYngENqDOk6kk3Gy39UExft+oXV35+DVEQYLyb4aRpr4ddCuAJ/K6EHXesSWVTpmv8mrM0fhqLP5hNG2h3pxe3URsLzTwOPlAgMBAAECggEAWwsklctyX98FAlF/wU8LE9ktXY/+yHpPJDxCbQ0Q3Y2eckTyIdgZEHSrp6PqZ6F56O2hPIw7GMRIaWh9Ip2+REfjYsw92ReQsUmXn2SgHXxPXtdkv4IPxo/mLsJ8MWjAHALpzZCkHr/lxapgmUJbtRotd7KWFqt62CHtNgYKWvhJQqpjL/04029L6PGELMrVCyTFPw8M7buay/dFZAErhvcc//PEbUFR4tfaQzx2pLY+HFsuTF97RjSXiW6g251RnJY7d6MDpc58mprJ1XAr4evCFVye+mrx+5Rg8hKv+Ffo871CnKihQXXajl3tgb3bHF94OTL5O6oVohCdWxuuRQKBgQDcpST7Om3Kl2U/A1tcRn1v096RlwbA1qsoYsMKkpCS0Q5p2FYNqCXBQr1dBdqjRaqklLdfIM7WxTH9eUiDwSv8bPrv0nf3PckOq39o7Hz6SX/KOi0SwbSo1yF4OEZrmhN8gB1bAcF9c6A9eT/8v9nuMr5wJLcXliixPDwi+9mdwwKBgQDI2k9i9mrVQqWkc5hriE2+AAL3B3bI0FxmOgAlbXu9yp7z0fat/czAKY9aWUJkauPTq4gLYVLWG+n+sa3feOb4Ge6Pi0EnEni6slLDFBx+ntJhGnoXn0tYcEt7E1qv8Da0z3yPcDuqKGB4ahFzG9c/kbWVV0rHaz3W/OpWPcUVNwKBgA6QgB5tPu4OjEpaiF9Q79q/24M0uudjxCyISGOcRRrL3e2ysAt6u/ND2ogXaNU6NgdjIa+P1NdscN5QcS7xQ1wIPZAzDmQtmeX0ABcG9UD4NQ0+RddlHe2/NaBf5pYXt42ST13AgessBLnKFSUz7MjCZLu2ULen28vHNBkDEtxNAoGBALHYjJ7GLwaywtUA/h+JS71HaVivJUDfLlHeXa7GaOE+/sxKSDD8IEavBh4sFxuvghg1NHtPK/gmM2WwkpLkDS/2VkSIO6KLOpelup69uhVsMECHH0C3bhHbLRrHom9TNj2X02V3LX2XzwQQd28DYe8J6E1aQ/6cON+ahyzh+Ra1AoGAF7Fkfcs4UCCkxeY1IgQLJ+0c/VSY+aIv+dP4bXK1j/VGE+6B2PvcogTbP5VqGwMHzE6ulHfLE6ZKLeeRauEjB8WNFvvEvqmYez/ijO9bqUXucxc/tN/WEUesUXphtQrN6uVPXYTJZw7VvqPcPgNhKb/HzID+HWru8A0feUH9y/E=",
        //商户应用公钥,一行字符串
        'merchant_public_key'  => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArR0t8gWZ/LKe2ilEY9hPlW8C7KloFWYlaI0NFuFJf6iBWsueGRObjlgTKjyEzGaodNCML5sLUb+sM6vw12py0+PNmlDy04cf/wHAkAEQ3lHmm1tn6JR7r4Orw21Ygu/muQnxo5OpZF3To+5ejEu1Ceoq7e329neqpDm0uvyMkEQbx10tszy1Ch73o6VkhUrPZUVWpXWgn7AS4E7EB7txIjjHHq9uWBWDDQTk6NhPlpE1iP5or2/gL3mJ4BDagzpOpJNxst/VBMX7fqF1d+fg1REGC8m+Gkaa+HXQrgCfyuhB13rEllU6Zr/JqzNH4aiz+YTRtod6cXt1EbC808Dj5QIDAQAB",
        //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
        'alipay_public_key'    => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwly6doSyuH4CYaUcJBisu+UUTtqPTSPGmG5Wq/J0A1KnL06DIsLpH8Z0BsAB7bqnzPcbNREsW0hbZmwqdk/T9yv0a4MkIyLb+zk/q0bKRrD9ziqS0DS8Anz6XQtdfhqHa7OUd7wlORXWy4NlO7U5gJR0McGs0FFkZzPKbXYWXikC8f7d0Kpi9cbY2091VLFFkWOYZoSrFu9egSJHg4rv0ktzNCV3ULKvhHgC/c5+hH8LdpCnCLtB/RWQX3+Ra/zdpLR/Bbj8IwNMyjpzuwO9KS1JOuS4j6bq//iJ6+1fY2xT8TVoV2cyAA7n0q/7OzXt9WE4/gcYij8RFAqnzKyNXQIDAQAB",
        //编码格式只支持GBK。
        'charset'              => "GBK",
        //支付宝网关
        'gatewayUrl'           => "https://openapi.alipay.com/gateway.do",
        //签名方式
        'sign_type'            => "RSA2",
        'notify_url'=>'http://www.lubanit.com/api.php/Paycallback/alipayNotify',
        'notify_url_yajin'=>'http://www.lubanit.com/api.php/Paycallback/alipayNotifyYajin',
        'notify_url_yajin_frozen'=>'http://www.lubanit.com/api.php/Paycallback/alipayNotifyYajinFrozen',
    ],

];
