<?php

// [ 应用入口文件 ]
namespace think;
// 加载基础文件
require __DIR__ . '/system/thinkphp/base.php';

// 支持事先使用静态方法设置Request对象和Config对象
define('Token','d2f32alvvsfei2131oyn123jo22133');
// 执行应用并响应
Container::get('app')->path('app/')->bind('mobile')->run()->send();